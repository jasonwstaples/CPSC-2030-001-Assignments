-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 25, 2018 at 08:16 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost' IDENTIFIED BY PASSWORD '*F29CF4A1DE8DE0164B8FC2F1BE128965912C97EF' WITH GRANT OPTION;
--
-- Database: `chat_server`
--
CREATE DATABASE IF NOT EXISTS `chat_server` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `chat_server`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getLast10Messages` (IN `the_username` VARCHAR(100))  BEGIN
	SELECT * from messages 
    #WHERE username=the_username
    #AND time > DATE_ADD(now(), INTERVAL -1 HOUR)
    ORDER BY time desc limit 10;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getMessagesAfterTime` (IN `the_time` TIMESTAMP)  NO SQL
BEGIN
	SELECT * from messages 
    WHERE 
    time > the_time
    ORDER BY time desc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertMessage` (IN `the_username` VARCHAR(100), IN `the_message` VARCHAR(100))  BEGIN
	INSERT INTO messages (username,message) VALUES (the_username,the_message); 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(250) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`time`, `message`, `username`) VALUES
('2018-11-17 19:54:38', 'test msg', ''),
('2018-11-18 00:20:54', 'messagee2', ''),
('2018-11-18 00:49:26', 'test', 'user_1'),
('2018-11-18 00:49:37', 'test', 'user_1'),
('2018-11-19 00:28:56', 'jjjj', 'user_1'),
('2018-11-19 00:29:05', 'kkkk', 'user_1'),
('2018-11-19 00:30:08', 'kkkk', 'user_2'),
('2018-11-19 00:33:37', 'ttt', 'user_1'),
('2018-11-19 00:33:42', 'zzz', 'user_1'),
('2018-11-19 00:47:53', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:54', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:55', 'zzz', 'user_1'),
('2018-11-19 00:47:56', 'zzz', 'user_1'),
('2018-11-19 00:47:56', 'zzz', 'user_1'),
('2018-11-19 00:47:56', 'zzz', 'user_1'),
('2018-11-19 00:47:56', 'zzz', 'user_1'),
('2018-11-19 00:47:56', 'zzz', 'user_1'),
('2018-11-19 00:47:57', 'zzz', 'user_1'),
('2018-11-19 00:47:57', 'zzz', 'user_1'),
('2018-11-19 00:47:57', 'zzz', 'user_1'),
('2018-11-19 00:47:57', 'zzz', 'user_1'),
('2018-11-19 00:47:57', 'zzz', 'user_1'),
('2018-11-19 00:53:05', 'mnvb', 'user_1'),
('2018-11-19 17:01:30', 'lkjhlkjh', 'user_2'),
('2018-11-19 17:01:57', 'lkjhlkjh', 'user_2'),
('2018-11-19 17:10:01', 'another msg', 'user_2'),
('2018-11-19 17:10:03', 'another msg', 'user_2'),
('2018-11-19 17:10:04', 'another msg', 'user_2'),
('2018-11-19 18:01:44', 'gggg', 'user_1'),
('2018-11-19 18:02:48', 'xxx', 'user_1'),
('2018-11-19 18:07:20', '', 'user_1'),
('2018-11-24 20:21:40', 'hh', 'user_1'),
('2018-11-24 21:11:20', 'bbb', 'user_1'),
('2018-11-24 21:16:02', 'jjj', 'user_1'),
('2018-11-24 21:16:35', 'vvv', 'user_1'),
('2018-11-24 21:55:17', 'rrr', 'user_1'),
('2018-11-24 21:55:30', 'oooo', 'user_1'),
('2018-11-24 21:56:29', 'nnnnnnnn', 'user_1'),
('2018-11-24 21:56:32', 'iiii', 'user_1'),
('2018-11-25 03:01:18', 'xxxxxxxxxx', 'user_2'),
('2018-11-25 03:12:03', 'kkkk', 'user_3'),
('2018-11-25 03:28:50', 'aaaa', 'user_1'),
('2018-11-25 03:44:29', '', 'user_1'),
('2018-11-25 03:47:06', 'uuuuuuuu', 'user_1'),
('2018-11-25 03:50:29', 'bb', 'user_1'),
('2018-11-25 03:51:17', 'ii', 'user_1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
