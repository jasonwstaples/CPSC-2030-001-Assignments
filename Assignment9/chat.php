<?php

require_once 'sqlhelper.php';
$conn = connectToMyDatabase(); //database setup code taken from Kim's course example




if(array_key_exists("get_last_10_msgs", $_POST) && array_key_exists("user", $_POST) ){//fetch 10 most recent message in the last hour  

    $username = $conn->real_escape_string($_POST['user']);
    $sql = "CALL getLast10Messages('".$username."')";
                           
    $result = $conn->query($sql);
    clearConnection($conn);

    if($result){
        
        $messages = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($messages);
            
    }else{
        echo "no result";
    }

}elseif(array_key_exists("add_message", $_POST) && array_key_exists("user", $_POST) && array_key_exists("message", $_POST)){

    //insert the msg
    $username = $conn->real_escape_string($_POST['user']);
    $message = $conn->real_escape_string($_POST['message']);

    $sql = "CALL insertMessage('".$username."','".$message."')";
                       
    $result = $conn->query($sql);
    clearConnection($conn);    

    $response = array("response"=>$_POST['message']);
    echo json_encode($response);

}elseif(array_key_exists("get_latest_msgs", $_POST) && array_key_exists("last_message_time", $_POST) ){//fetch all messages after the last_message_time  

    $time = $conn->real_escape_string($_POST['last_message_time']);
    $sql = "CALL getMessagesAfterTime('".$time."')";
                           
    $result = $conn->query($sql);
    clearConnection($conn);

    if($result){
        
        $messages = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($messages);
            
    }else{
        echo "no result";
    }

}


?>