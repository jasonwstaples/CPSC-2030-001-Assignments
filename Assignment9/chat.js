let currentUser;


function createMessageDivs(row){
    let msg = "";
    
    msg = "<div class='row'><b>TIME:</b>&nbsp;&nbsp;&nbsp;"+row.time + "&nbsp;&nbsp;&nbsp;<b>USER:</b>&nbsp;&nbsp;&nbsp;" +row.username+ "&nbsp;&nbsp;&nbsp;<b>MESSAGE:</b>&nbsp;&nbsp;&nbsp;" +row.message + "</div>";
    
    return msg;
}



function getUserMessages(){//fetch 10 most recent message in the last hour

  let user = $("#username").val();
  console.log(user);
  
    $.ajax({
        url: "chat.php",
        method: "POST",
        data: {
            get_last_10_msgs : "true",
            user: user
        },
        success: function(msg){
          
          messageIndex = JSON.parse(msg);
          
          let output = "";  
          for(let i = 0; i < messageIndex.length; i++){
              output += createMessageDivs(messageIndex[i]);
          }
          //write the timestamp of the last msg recieved to a hidden field so it can be used again on the next poll for new msgs  
          $("#last_message_time").val(messageIndex[0].time);
          
          $(".message_list_last_hour").html(output);
        }
    })
}


function getLatestMessages(){//fetch 10 most recent message in the last hour
  //alert("getlatestmesages called");

  let user = $("#username").val();
  let last_message_time = $("#last_message_time").val();
  console.log(user);
  
    $.ajax({
        url: "chat.php",
        method: "POST",
        data: {
            get_latest_msgs : "true",
            user: user,
            last_message_time: last_message_time,
            
        },
        success: function(msg){
          console.log(msg);
          messageIndex = JSON.parse(msg);
          console.log(messageIndex.length);
          let output = "";  
          for(let i = 0; i < messageIndex.length; i++){
              console.log(messageIndex[i]);
              output += createMessageDivs(messageIndex[i]);
          }
  
          $("#last_message_time").val(messageIndex[0].time);
  
          //let existingMSGS = $(".message_list_last_hour").html();//get the existing messages
          let existingMSGS = $(".message_list").html();//get the existing messages
        
          $(".message_list").html(output+existingMSGS);//need to add the newest message div to the top of existing messages  
        }
    })

    
}


$(".login_user").click(function(event){//add click event to the login button

  getUserMessages();

}); 

$(".logout_user").click(function(event){//add click event to the login button

  //console.log("logout button clicked");
  let user = $("#username").val('');

});




function sendMsg(user,msg){

    $.ajax({
        url: "chat.php",
        method: "POST",
        data: {
            user: user,
            add_message : "true",
            message: msg,

        },
        success: function(msg){
          console.log(msg);
          getUserMessages();
        }
    })
}


$(".send_message").click(function(event){//add click even to the to send msg button

  //console.log("send msg button clicked");
  let user = $("#username").val();
  let msg = $("#message").val();

  sendMsg(user,msg);

}); 


let messageUpdater = setInterval(getLatestMessages, 10000);

$( document ).ready(function() {
    console.log("document ready");
    getUserMessages();
});





