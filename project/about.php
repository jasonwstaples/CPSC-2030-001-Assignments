<?php
session_start();

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<html>


  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    
  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      
      $username = '';
      if(!empty($_SESSION['user_id'])){
        $username = get_username($_SESSION['user_id']);
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","username"=>$username));

    ?>

    <?php 
      
      $sidebar = $twig->load('sidebar.twig.html');
      echo $sidebar->render(array("user_type"=>login_type())); 

    ?>

    <main id="mainfeature">

      <?php 
      
      $about = $twig->load('about.twig.html');
      echo $about->render(); 

      ?>
      
    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 
    ?>

  </body> 
</html>  

