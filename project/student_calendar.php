<?php

require_once 'security.php';

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<!doctype html>
  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    
  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      $username = '';
      if(!empty($_SESSION['user_id'])){
        $username = get_username($_SESSION['user_id']);
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","username"=>$username));



    ?>

    <?php 
     
      $sidebar = $twig->load('sidebar.twig.html');
      echo $sidebar->render(array("user_type"=>login_type())); 

     

    ?>

    <main id="mainfeature">

      <?php 


      $sql = "CALL getCourseList('student',".$_SESSION['user_id'].")";

                                                
      $course_list_result = $conn->query($sql);
      
      clearConnection($conn);

      $course_list = '';

      if($course_list_result){

        $course_list = $course_list_result->fetch_all(MYSQLI_ASSOC);

      } 


      $calendar = $twig->load('calendar.twig.html');
      

      if(login_type()=='customer'){//only customers allowed to use certain features of the calendar twig
          //echo "logged in user is a customer";
          echo $calendar->render(array("courses"=>$course_list,"user_type"=>'customer',"edit_id"=>0));        
      }  
      ?>

      

      
     
    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 

    ?>

  </body> 
</html>  






























