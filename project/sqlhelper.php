<?php

//You need this after running a SQL query that
//calls a stored procedure. For some reason, 
//procedure calls return multiple results, so the 
//extra result needs to be cleared.
//
//Example:
// $result = $conn->query("call getWeak('Ivysaur')");
// clearConnection($conn);

function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}


//DB setup for this web-app
function connectToMyDatabase(){
    $user = 'CPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost';
    $dbname = 'rogue_nation';


    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}
//twig setup for this web-app
function setupMyTwigEnvironment(){
    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
    $twig = new Twig_Environment($loader); 
    return $twig;  
}

function dumpErrorPage($twig){
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
}


 //$conn = connectToMyDatabase();//ensure a database connection object is available
function login_type(){

    if(empty($_SESSION['user_id'])){
        return false;

    }else{
        $conn = connectToMyDatabase();
        $sql = "CALL getUserByID(".$_SESSION['user_id'].")";

                                      
      $user_result = $conn->query($sql);
      
      clearConnection($conn);

      if($user_result){

        $user =  $user_result->fetch_assoc();
       
        return $user['user_type'];
        
      } 

    }
}


function get_username(){

    if(empty($_SESSION['user_id'])){
        return false;

    }else{
        $conn = connectToMyDatabase();
        $sql = "CALL getUserByID(".$_SESSION['user_id'].")";

                                      
      $user_result = $conn->query($sql);
      
      clearConnection($conn);

      if($user_result){

        $user =  $user_result->fetch_assoc();
        //print_r($user);
        return $user['username'];
        
      } 

    }
}
?>

