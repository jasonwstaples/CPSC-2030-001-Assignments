<?php

require_once 'security.php';


if(login_type()<>'admin'){//only admins allowed to use this page
  header("Location: login.php?login_failed=1");
  die();        
}  

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<!doctype html>

  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    

<script type="text/javascript">


</script>

  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      $username = '';
      if(!empty($_SESSION['user_id'])){
        $username = get_username($_SESSION['user_id']);
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","username"=>$username));

    ?>

    <?php 
      
      $sidebar = $twig->load('sidebar.twig.html');
       echo $sidebar->render(array("user_type"=>login_type())); 

    ?>

    <main id="mainfeature">

      <div id="responsecontainer" align="center">

      </div>  

      <?php 

    
      $sql = "CALL getCourseList('',0)";       
          
      $course_list_result = $conn->query($sql);
      clearConnection($conn);

      $course_list = '';

      if($course_list_result){
        $course_list =  $course_list_result->fetch_all(MYSQLI_ASSOC);
      } 

      $edit_id = 0;
      if(!empty($_REQUEST['edit_id'])){

        $edit_id = $conn->real_escape_string($_REQUEST['edit_id']);
        
      }

      $sidebar = $twig->load('calendar.twig.html');
      echo $sidebar->render(array("courses"=>$course_list,"user_type"=>'admin',"edit_id"=>$edit_id)); 
      ?>

    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 

    ?>

  </body> 
</html>  






























