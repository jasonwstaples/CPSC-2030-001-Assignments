<?php
session_start();

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<html>


  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    
  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      
      $username = '';
      if(!empty($_SESSION['user_id'])){
        $username = get_username($_SESSION['user_id']);
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","username"=>$username));

    ?>

    <?php 
      
      $sidebar = $twig->load('sidebar.twig.html');
      echo $sidebar->render(array("user_type"=>login_type())); 

    ?>

    <main id="mainfeature">
      <h2>Train with Ethan Hunt!</h2>

      <div>If you are looking for motorcycle driver training why not train with the best?</div>
      <div>Don't just learn to drive a motorcycle, learn to ride at 200mph, learn to jump cars, learn to ride to survive!!!</div>
      <div class="bike_chase"></div>
      <!--<img src="images/bike_chase.jpg" alt="">--><!-- https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiy0Zra_czdAhXxJDQIHVXNBhwQjRx6BAgBEAU&url=http%3A%2F%2Fwordpress.rideapart.com%2F2018%2F07%2Fmission-impossible-fallout-motorcycle-stunts%2F&psig=AOvVaw3vSD1Bv1voiddFeg2HuWTf&ust=1537648908196183-->
    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 
    ?>

  </body> 
</html>  

