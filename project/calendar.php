<?php

//require_once 'security.php';
session_start();



require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<!doctype html>
  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    
  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      $user_id = '';
      if(!empty($_SESSION['user_id'])){
        $user_id = $_SESSION['user_id'];
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","user_id"=>$user_id));



    ?>

    <?php 
      
      $sidebar = $twig->load('sidebar.twig.html');
      echo $sidebar->render(); 

    ?>

    <main id="mainfeature">

      <?php 


      $sql = "CALL getCourseList('student',0)";

                                                
      $course_list_result = $conn->query($sql);
      
      clearConnection($conn);

      $course_list = '';

      if($course_list_result){

        $course_list =  $course_list_result->fetch_all(MYSQLI_ASSOC);

       } 

      $sidebar = $twig->load('calendar.twig.html');
      echo $sidebar->render(array("courses"=>$course_list)); 
        

      


      ?>
       <script>
        //alert('running');
        //$("#calendar_header_3").removeClass("calendar_header_3");
        $(".calendar_header_3").css("width", "40%");
        $(".calendar_column_3").css("width", "40%");
   
       </script>


    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 

    ?>

  </body> 
</html>  






























