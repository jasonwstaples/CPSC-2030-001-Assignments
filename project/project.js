function special_goodbye(event){

    $(".special_goodbye").show();
   	
    $("#special_goodbye").addClass("special_goodbye");
   
    console.log('special goodbye called');
}
    
$(".logout_button").click(special_goodbye);



$('.special_goodbye').bind("animationend", (event)=>{ //need to use the animationend event for opening the url because it happens too fast otherwise!
			 console.log('special goodbye animationend called');
			 alert('Goodbye, thanks for logging in to Ethan Hunt Motorcycle Training School!');
 			 $(".special_goodbye").hide();
            $(location).attr('href',"security.php?logout=1");
});

function validateLogin(){
		console.log(validateLogin);
		let username = $("#username").val();
		let password = $("#password").val();
		if(!username){
			alert("You have not entered a username value, please try again");
		}

		if(!password){
			alert("You have not entered a password value, please try again");
		}

	}


function deleteCourse(course_id){
	console.log("deleteCourse called with course id"+course_id);

	 $("#calendar_row_"+course_id).hide();//hide the row that will be deleted from the user

	 $.ajax({
        url: "ajax/delete_course.php",
        method: "POST",
        data: {
            action : "delete",
            course_id: course_id
        },
        success: function(msg){
          
          console.log(msg);
        }
    })

}


function editCourse(course_id){
	console.log('editCourse called');
	$("#course_date_field_"+course_id).show();
	$("#course_date_label_"+course_id).hide();
	$("#course_name_field_"+course_id).show();
	$("#course_name_label_"+course_id).hide();
	$("#course_location_field_"+course_id).show();
	$("#course_location_label_"+course_id).hide();
	$("#course_status_field_"+course_id).show();
	$("#course_status_label_"+course_id).hide();


	$("#update_button_"+course_id).show();
	$("#delete_button_"+course_id).hide();
	$("#edit_button_"+course_id).hide();

}


function updateCourse(course_id){
	console.log('update Course called');

	let course_date = $("#course_date_field_"+course_id).val();
	let course_name = $("#course_name_field_"+course_id).val();
	let course_location = $("#course_location_field_"+course_id).val();
	let course_status = $("#course_status_field_"+course_id).val();
	console.log("course status is"+course_status);

	if(!course_date){
		alert("please enter a course date");
		return;
	}

	if(!course_name){
		alert("please enter a course name");
		return;
	}

	if(!course_location){
		alert("please enter a course location");
		return;
	}

	 $.ajax({
        url: "ajax/update_course.php",
        method: "POST",
        data: {
            action : "update",
            course_id: course_id,
            course_date: course_date,
            course_name: course_name,
            course_location: course_location,
            status: course_status
        },
        success: function(msg){
          
          console.log(msg);

        }
    })

	 $.ajax({
	 	 //url: "course_ajax.php",
        url: "ajax/get_courses.php",
        method: "POST",
        data: {
            action : "get_all_courses"
            
        },
        success: function(msg){
          
         	//console.log(msg);

          	messageIndex = JSON.parse(msg);
        
          	let output = "";  
          	for(let i = 0; i < messageIndex.length; i++){
          		//console.log("looping through msgs");
              	output += createMessageDivs(messageIndex[i]);
              	
        	}


			$(".course_list").empty();
			$(".course_list_added").html(output);

			applyEnrollHovers();     


 		}
    })

	
	
}


function addCourse(course_id){
	//console.log("addCourse called");

	
	let course_date = $("#course_date").val();
	let course_name = $("#course_name").val();
	let course_location = $("#course_location").val();
	let course_status = $("#course_status").val();
	console.log(course_status);

	if(!course_date){
		alert("please enter a course date");
		return;
	}

	if(!course_name){
		alert("please enter a course name");
		return;
	}

	if(!course_location){
		alert("please enter a course location");
		return;
	}

	 $.ajax({
        url: "ajax/add_course.php",
        method: "POST",
        data: {
            action : "add",
            course_date: course_date,
            course_name: course_name,
            course_location: course_location,
            status: course_status
        },
        success: function(msg){
          
          //console.log(msg);

        }
    })

	 $.ajax({
        url: "ajax/get_courses.php",
        method: "POST",
        data: {
            action : "get_all_courses"
            
        },
        success: function(msg){
          
         	console.log(msg);

          	messageIndex = JSON.parse(msg);
         
          	let output = "";  
          	for(let i = 0; i < messageIndex.length; i++){
          		//console.log("looping through msgs");
              	output += createMessageDivs(messageIndex[i]);
              	
        	}



			$(".course_list").empty();
			$(".course_list_added").html(output);	   


			applyEnrollHovers();      
        	
        

        }
    })

	


}


function createMessageDivs(row){
    	let divs = "";
    


		divs="<div class='calendar_row' id='calendar_row_"+row.id+"'>"+
				"<div class='calendar_column_1'>"+
					"<div id='course_date_label_"+row.id+"'>"+row.start_date+"</div>"+   
					"<input type='text' class ='hidden_field' name='course_date' id ='course_date_field_"+row.id+"' value='"+row.start_date+"'>"+ 
					"<div class ='num_enrolled hidden_field' id = 'num_enrolled_"+row.id+"'># of Students Enrolled: "+row.num_enrolled+"</div>"+           
				"</div>"+  
				"<div class='calendar_column_2'>"+
					"<div id='course_name_label_"+row.id+"'>"+row.name+"</div>"+
						"<input type='text' class ='hidden_field name='course_name' id ='course_name_field_"+row.id+"'' value='"+row.name+"'>"+
				"</div>"+
				"<div class='calendar_column_3'>"+
					"<div id='course_location_label_"+row.id+"'>"+row.location+"</div><div class='display_status' id='course_status_label_"+row.id+"'>"+row.display_status+"</div>"+
					"<input type='text' class ='hidden_field' name='course_location'  id ='course_location_field_"+row.id+"'value='"+row.location+"'>"+
					"<div id='course_status_label_"+row.id+"'>"+
						"<select class ='hidden_field' name='status' id ='course_status_field_"+row.id+"'>"+
						  	"<option value='1'>active</option>"+
						  	"<option value='0'>inactive</option>"+
						"</select>"+  
					"</div>"+	
				"</div>"+
				"<div class='calendar_column_4'>"+
					"<input type='button' class='hidden_field' value='update' id = 'update_button_"+row.id+"' onclick='updateCourse("+row.id+");'>"+			
					"<input type='button' value='edit' id = 'edit_button_"+row.id+"' onclick='editCourse("+row.id+");'>"+
					"<input type='button' value='delete' id = 'delete_button_"+row.id+"' onclick='deleteCourse("+row.id+");'>"+
				"</div>"+
			"</div>"	;

			     
    return divs;
}



function toggleEnrolled(id){
	//console.log("toggleEnrolled");
	if ($('#num_enrolled_'+id).hasClass("hidden_field")) {
        	$('#num_enrolled_'+id).removeClass("hidden_field");
    	}else{
    		$('#num_enrolled_'+id).addClass("hidden_field");
    	}
}


function applyEnrollHovers(){

	$( ".calendar_column_1" ).children().hover(function ( event ) {		
   		let target = $( event.target ).attr('id');
   		let myArray = target.split("_");
   		//console.log(myArray[3]);
   		let id = myArray[3];

   		toggleEnrolled(id);
	
    });	

    $( ".calendar_column_2" ).children().hover(function ( event ) {		
   	
   		let target = $( event.target ).attr('id');
   		let myArray = target.split("_");
   		//console.log(myArray[3]);
   		let id = myArray[3];

   		toggleEnrolled(id);
    });	

    $( ".calendar_column_3" ).children().hover(function ( event ) {		
   	
   		let target = $( event.target ).attr('id');
   		let myArray = target.split("_");
   		//console.log(myArray[3]);
   		let id = myArray[3];

   		toggleEnrolled(id);
    });	



}










