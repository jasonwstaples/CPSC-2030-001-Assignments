<?php

require_once '../security.php';

if(login_type()<>'admin'){
  header("Location: login.php?login_failed=1");
  die();        
}  

require_once '../sqlhelper.php';

$conn = connectToMyDatabase();

if(!empty($_REQUEST['action']) && $_REQUEST['action'] = 'delete' && !empty($_REQUEST['course_id'])){

		$course_id = $conn->real_escape_string($_REQUEST['course_id']);
        $sql = "CALL deleteCourse(".$course_id.")";       
//echo "<br>".$sql;                     
        $result = $conn->query($sql);
        clearConnection($conn);

        if($result){
       		echo '{"response":"success"}';
            
    	}else{
        	echo '{"response":"error"}';
    	}

}else{
	echo '{"response":"error"}';
}










?>
