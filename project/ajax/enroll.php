<?php

require_once '../security.php';


if(login_type()<>'customer'){
  header("Location: login.php?login_failed=1");
  die();        
}  


require_once '../sqlhelper.php';

$conn = connectToMyDatabase();

if(!empty($_REQUEST['enroll_id'])){
        $course_id = $conn->real_escape_string($_REQUEST['enroll_id']);

        $sql = "CALL enrollCourse(".$_SESSION['user_id'].",".$course_id.")";  
        echo "<br>".$sql;                  
        $enroll_result = $conn->query($sql);
        clearConnection($conn);

        header("Location: ..//student_calendar.php");
  		die();   
}

if(!empty($_REQUEST['unenroll_id'])){
        $course_id = $conn->real_escape_string($_REQUEST['unenroll_id']);

        $sql = "CALL unEnrollCourse(".$_SESSION['user_id'].",".$course_id.")";  
        echo "<br>".$sql;                  
        $unenroll_result = $conn->query($sql);
        clearConnection($conn);

        header("Location: ../student_calendar.php");
  		die();   
}



?>

<div>


</div>	