<?php

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$conn = connectToMyDatabase();

?>

<!doctype html>
  <?php
      $meta_info = $twig->load('meta_info.twig.html');
      echo $meta_info->render(array("page_title"=>"Ethan Hunt Motorcycle Training Academy")); 
  ?>    
  <body>
    <?php 
     
      $header = $twig->load('header.twig.html');
      $user_id = '';
      if(!empty($_SESSION['user_id'])){
        $user_id = $_SESSION['user_id'];
      }

      echo $header->render(array("title"=>"Ethan Hunt Motorcycle Training Academy","user_id"=>$user_id));



    ?>

    <?php 
      
      $sidebar = $twig->load('sidebar.twig.html');
      echo $sidebar->render(); 

    ?>

    <main id="mainfeature">

      <?php

      
        $login = $twig->load('create_account.twig.html');
        echo $login->render(array("action_message"=>"Create Account")); 
     
        
       
      ?>
     
    </main>
    
    <?php 
      
      $footer = $twig->load('footer.twig.html');
      echo $footer->render(array()); 

    ?>

  </body> 
</html>  

