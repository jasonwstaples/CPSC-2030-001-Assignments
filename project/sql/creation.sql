-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 04, 2018 at 03:22 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rogue_nation`
--
CREATE DATABASE IF NOT EXISTS `rogue_nation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `rogue_nation`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addUser` (IN `the_username` VARCHAR(100), IN `the_password` VARCHAR(100), IN `the_user_type` VARCHAR(100))  BEGIN

INSERT INTO user (username,password,user_type,active_status) VALUES (the_username,the_password,the_user_type,0);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addUserPayment` (IN `the_paymentToken` VARCHAR(100), IN `userID` INT)  BEGIN
INSERT INTO payment (payment_token)
    VALUES(the_paymentToken);         
INSERT INTO user_payment_lnk(payment_id,user_id)
    VALUES(LAST_INSERT_ID(),userID);  # use ID in second table
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteCourse` (IN `course_id` INT)  BEGIN
DELETE FROM course where id = course_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `enrollCourse` (IN `the_user_id` INT, IN `the_course_id` INT)  BEGIN
   INSERT INTO course_enrollment_lnk (user_id,course_id) VALUES (the_user_id,the_course_id);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCourse` (IN `course_id` INT)  BEGIN
SELECT * FROM course where id = course_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCourseList` (IN `list_type` VARCHAR(100), IN `the_user_id` INT)  BEGIN

	IF list_type = 'student' THEN
    	IF the_user_id > 0 THEN
   		
       		SELECT course.id, course.name, course.start_date, course.status, course.location, lnk.user_id FROM course left join course_enrollment_lnk as lnk on course.id=lnk.course_id AND lnk.user_id = the_user_id WHERE status = 1 ORDER BY start_date;
        ELSE    
        	SELECT * FROM course WHERE status = 1 ORDER BY start_date;
        END IF;
    ELSE
    	SELECT id,location,name,
(select count(*) from course_enrollment_lnk where course_enrollment_lnk.course_id = course.id) as num_enrolled,
start_date,status ,IF(status=1,'active','inactive') as display_status FROM course ORDER BY start_date;
    END IF;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUser` (IN `the_username` VARCHAR(100), IN `the_password` VARCHAR(100))  BEGIN
SELECT * FROM user where username = the_username and password = the_password;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByID` (IN `user_id` INT)  BEGIN
SELECT * FROM user where id = user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCourse` (IN `courseName` VARCHAR(255), IN `startDate` DATE, IN `a_status` INT, IN `a_location` VARCHAR(100))  BEGIN
   INSERT INTO course (name,start_date,status,location) VALUES (courseName,startDate,a_status,a_location);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `unEnrollCourse` (IN `the_user_id` INT, IN `the_course_id` INT)  BEGIN
DELETE FROM course_enrollment_lnk where user_id = the_user_id and
course_id = the_course_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateCourse` (IN `courseName` VARCHAR(100), IN `startDate` DATE, IN `course_id` INT, IN `the_status` INT, IN `the_location` VARCHAR(100))  BEGIN
UPDATE course set name=courseName,start_date=startDate,status=the_status,location=the_location WHERE id=course_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserStatus` (IN `the_status` INT)  BEGIN

UPDATE user set active_status = the_status;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `start_date`, `status`, `location`) VALUES
(203, 'Course 1', '2018-12-04', 1, 'Vancouver'),
(205, 'Course 2', '2018-12-04', 1, 'Squamish'),
(206, 'Course 3', '2018-12-06', 1, 'Burnaby');

-- --------------------------------------------------------

--
-- Table structure for table `course_enrollment_lnk`
--

CREATE TABLE `course_enrollment_lnk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_enrollment_lnk`
--

INSERT INTO `course_enrollment_lnk` (`id`, `user_id`, `course_id`, `date_created`) VALUES
(38, 3, 203, '2018-12-03 18:18:08');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `payment_token` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `payment_token`, `date_created`) VALUES
(1, 'example_payment_token_1', '2018-10-07 13:27:10');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `active_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `active_status`, `payment_status`, `user_type`, `date_created`) VALUES
(1, 'admin', 'admin', 1, 1, 'admin', '2018-10-07 13:14:21'),
(3, 'student1', 'student1', 1, 1, 'customer', '2018-10-07 13:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment_lnk`
--

CREATE TABLE `user_payment_lnk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_payment_lnk`
--

INSERT INTO `user_payment_lnk` (`id`, `user_id`, `payment_id`) VALUES
(5, 3, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_enrollment_lnk`
--
ALTER TABLE `course_enrollment_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payment_lnk`
--
ALTER TABLE `user_payment_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `course_enrollment_lnk`
--
ALTER TABLE `course_enrollment_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_payment_lnk`
--
ALTER TABLE `user_payment_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
