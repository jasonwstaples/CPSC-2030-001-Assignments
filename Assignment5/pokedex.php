<html>
	<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <title>CPSC 2030 - Assignment 5 - Jason Staples - Langara ID# 100269858</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet/less" type="text/css" media="screen" href="pokedex.less" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>

    </head>


    <body>

            <?php require 'db_connection.php'; ?>

            <a href="pokedex.php">POKEDEX HOME</a>
            <br><br><br>
            <div class="deck">

                <?php
                $sql = "CALL get_pokemon_list(0)";
                IF(empty($_GET['type_id'])){
                     $sql = "CALL get_pokemon_list(0)";
                }ELSE{
                    $type_id = $conn->real_escape_string($_GET['type_id']);
                    $sql = 'CALL get_pokemon_list('.$type_id.')';
                }    

                $pokemon_result = $conn->query($sql);
                clearConnection($conn);

                if($pokemon_result){
                    $table = $pokemon_result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays
                                                               
                    foreach( $table as $row){

                        echo '<div class = "card">';
                            
                        echo '<div><div class ="label">Pokedex Number:</div> '.htmlentities($row["national_pokedex_number"]).'</div>';    
                        
                        echo '<div><div class ="label">Name:</div> <a href="pokemon.php?name='.urlencode($row["name"]).'&number='.urlencode($row["national_pokedex_number"]).'">'.htmlentities($row["name"]).'</a></div>';


                        $pokemon_id = $conn->real_escape_string($row["pokemon_id"]);

                        $types_sql = 'CALL get_pokemon_types('.$pokemon_id.')';
                            
                       
                            
                        $types_result = $conn->query($types_sql);
                        
                        if($types_result){
                            echo '<div class ="label">TYPES:</div>';

                            $table2 = $types_result->fetch_all(MYSQLI_ASSOC);
                            clearConnection($conn);
                           
                            foreach( $table2 as $type_row){
                                echo '<div class="pokemon_type"><a href="pokedex.php?type_id='.urlencode($type_row["type_id"]).'">'.htmlentities($type_row["type_name"]).'</a></div>';     
                            }   
                        } 

                        //echo mysqli_error($conn);
                        echo '</div>';//close the card div


                    }

                } else {
                       echo "No Pokemon Found";
                       echo mysqli_error($conn);
                }

                	


                $conn->close();
                ?>

            </div>
    </body>            

</html>