<?php

$servername = "localhost:4001";
$username = "CPSC2030";
$password = "CPSC2030";
$database = "pokedex";
// Create connection
$conn = new mysqli($servername, $username, $password,$database);
// Check connection
if ($conn->connect_error) {    
	echo "<br><br><h2><b>Connection Error, please check the value for localhost in db_connection.php -> the school lab computer requires port 4001, yours may not</b></h2><br><br>";
	die("Connection failed: " . $conn->connect_error);
	exit;

}

error_reporting(E_ALL ^ E_WARNING);

//HELPER Database function provided by Kim Lam
//You need this after running a SQL query that
//calls a stored procedure. For some reason, 
//procedure calls return multiple results, so the 
//extra result needs to be cleared.
//
//Example:
// $result = $conn->query("call getWeak('Ivysaur')");
// clearConnection($conn);
//Got this function from Kim in the provided course examples

function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
} 



?>