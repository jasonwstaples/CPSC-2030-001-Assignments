<html>
	<head>
	    <meta charset="utf-8" />
	        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	        <title>CPSC 2030 - Assignment 5 - Jason Staples - Langara ID# 100269858</title>
	        <meta name="viewport" content="width=device-width, initial-scale=1">
	        <link rel="stylesheet/less" type="text/css" media="screen" href="pokedex.less" />
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>

	</head>


	<body>

			<?php require 'db_connection.php'; ?>

			<a href="pokedex.php">POKEDEX HOME</a>
			<br><br><br>

			<div class="deck">
			<?php


			function output_types($conn,$sql){
			    //echo '<br>'.$sql;
			    $result = $conn->query($sql);
			    clearConnection($conn);
			    if($result){
			    	//echo "strong result exists";
			        $table = $result->fetch_all(MYSQLI_ASSOC);
			        foreach( $table as $row){
			           echo "<div class='pokemon_type'>".htmlentities($row["type_name"])."</div>";     
			        } 
			    }	  
			}



			if(!empty($_GET['name']) && !empty($_GET['number'])){
				$name = $conn->real_escape_string($_GET['name']);
				$number = $conn->real_escape_string($_GET['number']);

		
				$sql = "Call get_pokemon('".$name."',".$number.")";


				$pokemon_result = $conn->query($sql);
				clearConnection($conn);

				if($pokemon_result){

				$row =  $pokemon_result->fetch_assoc();


				?>    	
					<div class = "card">
				<?php    	

						
						echo '<div><div class ="label">Pokedex Number:</div> '.htmlentities($row["national_pokedex_number"]).'</div>';	
				    	echo '<div><div class ="label">Name:</div> '.htmlentities($row["name"]).'</div>';
						echo '<div><div class ="label">HP:</div> '.htmlentities($row["hp"]).'</div>';
						echo '<div><div class ="label">Attack:</div> '.htmlentities($row["attack"]).'</div>';
						echo '<div><div class ="label">Defense:</div> '.htmlentities($row["defense"]).'</div>';
						echo '<div><div class ="label">Sp. Atk:</div> '.htmlentities($row["special_attack"]).'</div>';
						echo '<div><div class ="label">Sp. Def:</div> '.htmlentities($row["special_defense"]).'</div>';
						echo '<div><div class ="label">Speed:</div> '.htmlentities($row["speed"]).'</div>';


						if($row["mega_evolution_id"] > 0){
							$sql = 'Call get_pokemon('.$row["mega_evolution_id"].')';
							$mega_evolution_result = $conn->query($sql);
							clearConnection($conn);

							if ($mega_evolution_result->num_rows > 0) {
								 $mega_form_row =  $mega_evolution_result->fetch_assoc();
								echo '<div><div class ="label">Mega Evolution Form:</div> '.htmlentities($mega_form_row["name"]).'</div>';
							}	
						}

						//Output Strong against, Weak against, Resistant To, and Vulnerable To.
						$types_sql = 'CALL get_pokemon_types('.$row['id'].')';
				         
				    	$types_result = $conn->query($types_sql);
				    
				    	if($types_result){
				        	echo '<div class ="label">TYPES:</div>';
				        	echo "<br>";	

				        	$table = $types_result->fetch_all(MYSQLI_ASSOC);
				        	clearConnection($conn);
				       
				        	foreach( $table as $type_row){
				            	echo '<div class="pokemon_type"><a href="pokedex.php?type_id='.urlencode($type_row["type_id"]).'">'.htmlentities($type_row["type_name"]).'</a></div>';     
				        	}   
				        	echo "<br>";	

				        	echo '<div class ="label">Strong Against: </div>';
				        	foreach( $table as $type_row){
				            	output_types($conn,'CALL get_strong_against_types('.$type_row["type_id"].')');
				        	} 
				        	echo "<br>";	
				        	echo '<div class ="label">Weak Against: </div>';
				        	foreach( $table as $type_row){
				            	output_types($conn,'CALL get_weak_against_types('.$type_row["type_id"].')');
				        	} 
				        	echo "<br>";		
				        	echo '<div class ="label">Resistant To: </div>';
				        	foreach( $table as $type_row){
				            	output_types($conn,'CALL get_resistant_to_types('.$type_row["type_id"].')');
				        	} 
				        	echo "<br>";	
				        	echo '<div class ="label">Vulnerable To: </div>';
				        	foreach( $table as $type_row){
				            	output_types($conn,'CALL get_vulnerable_to_types('.$type_row["type_id"].')');
				        	} 
				    	} 

				  
				?>   
					</div> 
				<?php

				}	

			}

			$conn->close();
			?>


			</div>

		</body>	

</html>