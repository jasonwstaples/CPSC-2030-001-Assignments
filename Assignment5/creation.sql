-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 20, 2018 at 11:59 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost' IDENTIFIED BY PASSWORD '*F29CF4A1DE8DE0164B8FC2F1BE128965912C97EF' WITH GRANT OPTION;

CREATE DATABASE pokedex;
--
-- Database: `pokedex`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_pokemon` (IN `the_name` VARCHAR(100), IN `number` INT)  BEGIN

SELECT * from pokemon where name = the_name
AND national_pokedex_number = number;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_pokemon_list` (IN `type_id` INT)  BEGIN

 IF type_id > 0 THEN
 	SELECT p.id as pokemon_id,name,national_pokedex_number,hp,attack,defense,special_attack,special_defense,speed,mega_evolution_id,type_id from pokemon p INNER JOIN pokemon_types_lnk ptk ON p.id = ptk.pokemon_id WHERE ptk.type_id = type_id ORDER BY national_pokedex_number ASC;
 ELSE 
 	SELECT id as pokemon_id,name,national_pokedex_number,hp,attack,defense,special_attack,special_defense,speed,mega_evolution_id from pokemon ORDER BY national_pokedex_number ASC;
 END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_pokemon_types` (IN `pokemon_id` INT)  SELECT t.id as type_id,t.type_name from pokemon_types_lnk ptk
	INNER JOIN type t on ptk.type_id=t.id
    WHERE ptk.pokemon_id=pokemon_id
    GROUP BY type_name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_resistant_to_types` (IN `type_id` INT)  NO SQL
select type_name from type INNER JOIN resistant_to_type_lnk rto on type.id = rto.resistant_to_type_id WHERE rto.type_id = type_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_strong_against_types` (IN `type_id` INT)  select type_name from type INNER JOIN strong_against_type_lnk sat on type.id = sat.strong_against_type_id WHERE sat.type_id = type_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_vulnerable_to_types` (IN `type_id` INT)  select type_name from type INNER JOIN vulnerable_to_type_lnk vto on type.id = vto.vulnerable_to_type_id WHERE vto.type_id = type_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_weak_against_types` (IN `type_id` INT)  select type_name from type INNER JOIN weak_against_type_lnk wat on type.id = wat.weak_against_type_id WHERE wat.type_id = type_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `id` int(11) NOT NULL,
  `national_pokedex_number` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `hp` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defense` int(11) NOT NULL,
  `special_attack` int(11) NOT NULL,
  `special_defense` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  `mega_evolution_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`id`, `national_pokedex_number`, `name`, `type`, `hp`, `attack`, `defense`, `special_attack`, `special_defense`, `speed`, `mega_evolution_id`) VALUES
(8, 387, 'Turtwig', 'GRASS', 55, 68, 64, 45, 55, 31, 0),
(9, 388, 'Grotle', 'GRASS', 75, 89, 85, 55, 65, 36, 0),
(10, 389, 'Torterra', 'GRASSGROUND', 95, 109, 105, 75, 85, 56, 0),
(11, 390, 'Chimchar', 'FIRE', 44, 58, 44, 58, 44, 61, 0),
(12, 391, 'Monferno', 'FIREFIGHT', 64, 78, 52, 78, 52, 81, 0),
(13, 392, 'Infernape', 'FIREFIGHT', 76, 104, 71, 104, 71, 108, 0),
(14, 393, 'Piplup', 'WATER', 53, 51, 53, 61, 56, 40, 0),
(15, 394, 'Prinplup', 'WATER', 64, 66, 68, 81, 76, 50, 0),
(16, 395, 'Empoleon', 'WATERSTEEL', 84, 86, 88, 111, 101, 60, 0),
(17, 396, 'Starly', 'NORMALFLYING', 40, 55, 30, 30, 30, 60, 0),
(18, 397, 'Staravia', 'NORMALFLYING', 55, 75, 50, 40, 40, 80, 0),
(19, 398, 'Staraptor', 'NORMALFLYING', 85, 120, 70, 50, 60, 100, 0),
(20, 399, 'Bidoof', 'NORMAL', 59, 45, 40, 35, 40, 31, 0),
(21, 400, 'Bibarel', 'NORMALWATER', 79, 85, 60, 55, 60, 71, 0),
(22, 401, 'Kricketot', 'BUG', 37, 25, 41, 25, 41, 25, 0),
(23, 402, 'Kricketune', 'BUG', 77, 85, 51, 55, 51, 65, 0),
(24, 403, 'Shinx', 'ELECTR', 45, 65, 34, 40, 34, 45, 0),
(25, 404, 'Luxio', 'ELECTR', 60, 85, 49, 60, 49, 60, 0),
(26, 405, 'Luxray', 'ELECTR', 80, 120, 79, 95, 79, 70, 0),
(27, 406, 'Budew', 'GRASSPOISON', 40, 30, 35, 50, 70, 55, 0),
(28, 407, 'Roserade', 'GRASSPOISON', 60, 70, 65, 125, 105, 90, 0),
(29, 408, 'Cranidos', 'ROCK', 67, 125, 40, 30, 30, 58, 0),
(30, 409, 'Rampardos', 'ROCK', 97, 165, 60, 65, 50, 58, 0),
(31, 410, 'Shieldon', 'ROCKSTEEL', 30, 42, 118, 42, 88, 30, 0),
(32, 411, 'Bastiodon', 'ROCKSTEEL', 60, 52, 168, 47, 138, 30, 0),
(33, 412, 'Burmy', 'BUG', 40, 29, 45, 29, 45, 36, 0),
(34, 413, 'Wormadam', 'BUGGRASS', 60, 59, 85, 79, 105, 36, 0),
(35, 413, '\"Wormadam \r\n (Sandy Cloak)\"', 'BUGGROUND', 60, 79, 105, 59, 85, 36, 0),
(36, 413, '\"Wormadam \r\n (Trash Cloak)\"', 'BUGSTEEL', 60, 69, 95, 69, 95, 36, 0),
(37, 414, 'Mothim', 'BUGFLYING', 70, 94, 50, 94, 50, 66, 0),
(38, 415, 'Combee', 'BUGFLYING', 30, 30, 42, 30, 42, 70, 0),
(39, 416, 'Vespiquen', 'BUGFLYING', 70, 80, 102, 80, 102, 40, 0),
(40, 417, 'Pachirisu', 'ELECTR', 60, 45, 70, 45, 90, 95, 0),
(41, 418, 'Buizel', 'WATER', 55, 65, 35, 60, 30, 85, 0),
(42, 419, 'Floatzel', 'WATER', 85, 105, 55, 85, 50, 115, 0),
(43, 420, 'Cherubi', 'GRASS', 45, 35, 45, 62, 53, 35, 0),
(44, 421, 'Cherrim', 'GRASS', 70, 60, 70, 87, 78, 85, 0),
(45, 421, '\"Cherrim \r\n (Sunshine Form)\"', 'GRASS', 70, 60, 70, 87, 78, 85, 0),
(46, 422, 'Shellos', 'WATER', 76, 48, 48, 57, 62, 34, 0),
(47, 423, 'Gastrodon', 'WATERGROUND', 111, 83, 68, 92, 82, 39, 0),
(48, 424, 'Ambipom', 'NORMAL', 75, 100, 66, 60, 66, 115, 0),
(49, 425, 'Drifloon', 'GHOSTFLYING', 90, 50, 34, 60, 44, 70, 0),
(50, 426, 'Drifblim', 'GHOSTFLYING', 150, 80, 44, 90, 54, 80, 0),
(51, 427, 'Buneary', 'NORMAL', 55, 66, 44, 44, 56, 85, 0),
(52, 428, 'Lopunny', 'NORMAL', 65, 76, 84, 54, 96, 105, 53),
(53, 428, 'Mega Lopunny', 'NORMALFIGHT', 65, 136, 94, 54, 96, 135, 0),
(54, 429, 'Mismagius', 'GHOST', 60, 60, 60, 105, 105, 105, 0),
(55, 430, 'Honchkrow', 'DARKFLYING', 100, 125, 52, 105, 52, 71, 0),
(56, 431, 'Glameow', 'NORMAL', 49, 55, 42, 42, 37, 85, 0),
(57, 432, 'Purugly', 'NORMAL', 71, 82, 64, 64, 59, 112, 0),
(58, 433, 'Chingling', 'PSYCHC', 45, 30, 50, 65, 50, 45, 0),
(59, 434, 'Stunky', 'POISONDARK', 63, 63, 47, 41, 41, 74, 0),
(60, 435, 'Skuntank', 'POISONDARK', 103, 93, 67, 71, 61, 84, 0),
(61, 436, 'Bronzor', 'STEELPSYCHC', 57, 24, 86, 24, 86, 23, 0),
(62, 437, 'Bronzong', 'STEELPSYCHC', 67, 89, 116, 79, 116, 33, 0),
(63, 438, 'Bonsly', 'ROCK', 50, 80, 95, 10, 45, 10, 0),
(64, 439, 'Mime Jr.', 'PSYCHCFAIRY', 20, 25, 45, 70, 90, 60, 0),
(65, 440, 'Happiny', 'NORMAL', 100, 5, 5, 15, 65, 30, 0),
(66, 441, 'Chatot', 'NORMALFLYING', 76, 65, 45, 92, 42, 91, 0),
(67, 442, 'Spiritomb', 'GHOSTDARK', 50, 92, 108, 92, 108, 35, 0),
(68, 443, 'Gible', 'DRAGONGROUND', 58, 70, 45, 40, 45, 42, 0),
(69, 444, 'Gabite', 'DRAGONGROUND', 68, 90, 65, 50, 55, 82, 0),
(70, 445, 'Garchomp', 'DRAGONGROUND', 108, 130, 95, 80, 85, 102, 71),
(71, 445, 'Mega Garchomp', 'DRAGONGROUND', 108, 170, 115, 120, 95, 92, 0),
(72, 446, 'Munchlax', 'NORMAL', 135, 85, 40, 40, 85, 5, 0),
(73, 447, 'Riolu', 'FIGHT', 40, 70, 40, 35, 40, 60, 0),
(74, 448, 'Lucario', 'FIGHTSTEEL', 70, 110, 70, 115, 70, 90, 75),
(75, 448, 'Mega Lucario', 'FIGHTSTEEL', 70, 145, 88, 140, 70, 112, 0),
(76, 449, 'Hippopotas', 'GROUND', 68, 72, 78, 38, 42, 32, 0),
(77, 450, 'Hippowdon', 'GROUND', 108, 112, 118, 68, 72, 47, 0),
(78, 451, 'Skorupi', 'POISONBUG', 40, 50, 90, 30, 55, 65, 0),
(79, 452, 'Drapion', 'POISONDARK', 70, 90, 110, 60, 75, 95, 0),
(80, 453, 'Croagunk', 'POISONFIGHT', 48, 61, 40, 61, 40, 50, 0),
(81, 454, 'Toxicroak', 'POISONFIGHT', 83, 106, 65, 86, 65, 85, 0),
(82, 455, 'Carnivine', 'GRASS', 74, 100, 72, 90, 72, 46, 0),
(83, 456, 'Finneon', 'WATER', 49, 49, 56, 49, 61, 66, 0),
(84, 457, 'Lumineon', 'WATER', 69, 69, 76, 69, 86, 91, 0),
(85, 458, 'Mantyke', 'WATERFLYING', 45, 20, 50, 60, 120, 50, 0),
(86, 459, 'Snover', 'GRASSICE', 60, 62, 50, 62, 60, 40, 0),
(87, 460, 'Abomasnow', 'GRASSICE', 90, 92, 75, 92, 85, 60, 88),
(88, 460, 'Mega Abomasnow', 'GRASSICE', 90, 132, 105, 132, 105, 30, 0),
(89, 461, 'Weavile', 'DARKICE', 70, 120, 65, 45, 85, 125, 0),
(90, 462, 'Magnezone', 'ELECTRSTEEL', 70, 70, 115, 130, 90, 60, 0),
(91, 463, 'Lickilicky', 'NORMAL', 110, 85, 95, 80, 95, 50, 0),
(92, 464, 'Rhyperior', 'GROUNDROCK', 115, 140, 130, 55, 55, 40, 0),
(93, 465, 'Tangrowth', 'GRASS', 100, 100, 125, 110, 50, 50, 0),
(94, 466, 'Electivire', 'ELECTR', 75, 123, 67, 95, 85, 95, 0),
(95, 467, 'Magmortar', 'FIRE', 75, 95, 67, 125, 95, 83, 0),
(96, 468, 'Togekiss', 'FAIRYFLYING', 85, 50, 95, 120, 115, 80, 0),
(97, 469, 'Yanmega', 'BUGFLYING', 86, 76, 86, 116, 56, 95, 0),
(98, 470, 'Leafeon', 'GRASS', 65, 110, 130, 60, 65, 95, 0),
(99, 471, 'Glaceon', 'ICE', 65, 60, 110, 130, 95, 65, 0),
(100, 472, 'Gliscor', 'GROUNDFLYING', 75, 95, 125, 45, 75, 95, 0),
(101, 473, 'Mamoswine', 'ICEGROUND', 110, 130, 80, 70, 60, 80, 0),
(102, 474, 'Porygon-Z', 'NORMAL', 85, 80, 70, 135, 75, 90, 0),
(103, 475, 'Gallade', 'PSYCHCFIGHT', 68, 125, 65, 65, 115, 80, 104),
(104, 475, 'Mega Gallade', 'PSYCHCFIGHT', 68, 165, 95, 65, 115, 110, 0),
(105, 476, 'Probopass', 'ROCKSTEEL', 60, 55, 145, 75, 150, 40, 0),
(106, 477, 'Dusknoir', 'GHOST', 45, 100, 135, 65, 135, 45, 0),
(107, 478, 'Froslass', 'ICEGHOST', 70, 80, 70, 80, 70, 110, 0),
(108, 479, 'Rotom', 'ELECTRGHOST', 50, 50, 77, 95, 77, 91, 0),
(109, 479, '\"Rotom \r\n (Heat Rotom)\"', 'ELECTRFIRE', 50, 65, 107, 105, 107, 86, 0),
(110, 479, '\"Rotom \r\n (Wash Rotom)\"', 'ELECTRWATER', 50, 65, 107, 105, 107, 86, 0),
(111, 479, '\"Rotom \r\n (Frost Rotom)\"', 'ELECTRICE', 50, 65, 107, 105, 107, 86, 0),
(112, 479, '\"Rotom \r\n (Fan Rotom)\"', 'ELECTRFLYING', 50, 65, 107, 105, 107, 86, 0),
(113, 479, '\"Rotom \r\n (Mow Rotom)\"', 'ELECTRGRASS', 50, 65, 107, 105, 107, 86, 0),
(114, 480, 'Uxie', 'PSYCHC', 75, 75, 130, 75, 130, 95, 0),
(115, 481, 'Mesprit', 'PSYCHC', 80, 105, 105, 105, 105, 80, 0),
(116, 482, 'Azelf', 'PSYCHC', 75, 125, 70, 125, 70, 115, 0),
(117, 483, 'Dialga', 'STEELDRAGON', 100, 120, 120, 150, 100, 90, 0),
(118, 484, 'Palkia', 'WATERDRAGON', 90, 120, 100, 150, 120, 100, 0),
(119, 485, 'Heatran', 'FIRESTEEL', 91, 90, 106, 130, 106, 77, 0),
(120, 486, 'Regigigas', 'NORMAL', 110, 160, 110, 80, 110, 100, 0),
(121, 487, 'Giratina', 'GHOSTDRAGON', 150, 100, 120, 100, 120, 90, 0),
(122, 487, '\"Giratina \r\n (Origin Forme)\"', 'GHOSTDRAGON', 150, 120, 100, 120, 100, 90, 0),
(123, 488, 'Cresselia', 'PSYCHC', 120, 70, 120, 75, 130, 85, 0),
(124, 489, 'Phione', 'WATER', 80, 80, 80, 80, 80, 80, 0),
(125, 490, 'Manaphy', 'WATER', 100, 100, 100, 100, 100, 100, 0),
(126, 491, 'Darkrai', 'DARK', 70, 90, 90, 135, 90, 125, 0),
(127, 492, 'Shaymin', 'GRASS', 100, 100, 100, 100, 100, 100, 0),
(128, 492, '\"Shaymin \r\n (Sky Forme)\"', 'GRASSFLYING', 100, 103, 75, 120, 75, 127, 0),
(129, 493, 'Arceus', 'NORMAL', 120, 120, 120, 120, 120, 120, 0),
(130, 494, 'Victini', 'PSYCHCFIRE', 100, 100, 100, 100, 100, 100, 0),
(131, 495, 'Snivy', 'GRASS', 45, 45, 55, 45, 55, 63, 0),
(132, 496, 'Servine', 'GRASS', 60, 60, 75, 60, 75, 83, 0),
(133, 497, 'Serperior', 'GRASS', 75, 75, 95, 75, 95, 113, 0),
(134, 498, 'Tepig', 'FIRE', 65, 63, 45, 45, 45, 45, 0),
(135, 499, 'Pignite', 'FIREFIGHT', 90, 93, 55, 70, 55, 55, 0),
(136, 500, 'Emboar', 'FIREFIGHT', 110, 123, 65, 100, 65, 65, 0),
(137, 501, 'Oshawott', 'WATER', 55, 55, 45, 63, 45, 45, 0),
(138, 502, 'Dewott', 'WATER', 75, 75, 60, 83, 60, 60, 0),
(139, 503, 'Samurott', 'WATER', 95, 100, 85, 108, 70, 70, 0),
(140, 504, 'Patrat', 'NORMAL', 45, 55, 39, 35, 39, 42, 0),
(141, 505, 'Watchog', 'NORMAL', 60, 85, 69, 60, 69, 77, 0),
(142, 506, 'Lillipup', 'NORMAL', 45, 60, 45, 25, 45, 55, 0),
(143, 507, 'Herdier', 'NORMAL', 65, 80, 65, 35, 65, 60, 0),
(144, 508, 'Stoutland', 'NORMAL', 85, 110, 90, 45, 90, 80, 0),
(145, 509, 'Purrloin', 'DARK', 41, 50, 37, 50, 37, 66, 0),
(146, 510, 'Liepard', 'DARK', 64, 88, 50, 88, 50, 106, 0),
(147, 511, 'Pansage', 'GRASS', 50, 53, 48, 53, 48, 64, 0),
(148, 512, 'Simisage', 'GRASS', 75, 98, 63, 98, 63, 101, 0),
(149, 513, 'Pansear', 'FIRE', 50, 53, 48, 53, 48, 64, 0),
(150, 514, 'Simisear', 'FIRE', 75, 98, 63, 98, 63, 101, 0),
(151, 515, 'Panpour', 'WATER', 50, 53, 48, 53, 48, 64, 0),
(152, 516, 'Simipour', 'WATER', 75, 98, 63, 98, 63, 101, 0),
(153, 517, 'Munna', 'PSYCHC', 76, 25, 45, 67, 55, 24, 0),
(154, 518, 'Musharna', 'PSYCHC', 116, 55, 85, 107, 95, 29, 0),
(155, 519, 'Pidove', 'NORMALFLYING', 50, 55, 50, 36, 30, 43, 0),
(156, 520, 'Tranquill', 'NORMALFLYING', 62, 77, 62, 50, 42, 65, 0),
(157, 521, 'Unfezant', 'NORMALFLYING', 80, 115, 80, 65, 55, 93, 0),
(158, 522, 'Blitzle', 'ELECTR', 45, 60, 32, 50, 32, 76, 0),
(159, 523, 'Zebstrika', 'ELECTR', 75, 100, 63, 80, 63, 116, 0),
(160, 524, 'Roggenrola', 'ROCK', 55, 75, 85, 25, 25, 15, 0),
(161, 525, 'Boldore', 'ROCK', 70, 105, 105, 50, 40, 20, 0),
(162, 526, 'Gigalith', 'ROCK', 85, 135, 130, 60, 80, 25, 0),
(163, 527, 'Woobat', 'PSYCHCFLYING', 55, 45, 43, 55, 43, 72, 0),
(164, 528, 'Swoobat', 'PSYCHCFLYING', 67, 57, 55, 77, 55, 114, 0),
(165, 529, 'Drilbur', 'GROUND', 60, 85, 40, 30, 45, 68, 0),
(166, 530, 'Excadrill', 'GROUNDSTEEL', 110, 135, 60, 50, 65, 88, 0),
(167, 531, 'Audino', 'NORMAL', 103, 60, 86, 60, 86, 50, 168),
(168, 531, 'Mega Audino', 'NORMALFAIRY', 103, 60, 126, 80, 126, 50, 0),
(169, 532, 'Timburr', 'FIGHT', 75, 80, 55, 25, 35, 35, 0),
(170, 533, 'Gurdurr', 'FIGHT', 85, 105, 85, 40, 50, 40, 0),
(171, 534, 'Conkeldurr', 'FIGHT', 105, 140, 95, 55, 65, 45, 0),
(172, 535, 'Tympole', 'WATER', 50, 50, 40, 50, 40, 64, 0),
(173, 536, 'Palpitoad', 'WATERGROUND', 75, 65, 55, 65, 55, 69, 0),
(174, 537, 'Seismitoad', 'WATERGROUND', 105, 95, 75, 85, 75, 74, 0),
(175, 538, 'Throh', 'FIGHT', 120, 100, 85, 30, 85, 45, 0),
(176, 539, 'Sawk', 'FIGHT', 75, 125, 75, 30, 75, 85, 0),
(177, 540, 'Sewaddle', 'BUGGRASS', 45, 53, 70, 40, 60, 42, 0),
(178, 541, 'Swadloon', 'BUGGRASS', 55, 63, 90, 50, 80, 42, 0),
(179, 542, 'Leavanny', 'BUGGRASS', 75, 103, 80, 70, 80, 92, 0),
(180, 543, 'Venipede', 'BUGPOISON', 30, 45, 59, 30, 39, 57, 0),
(181, 544, 'Whirlipede', 'BUGPOISON', 40, 55, 99, 40, 79, 47, 0),
(182, 545, 'Scolipede', 'BUGPOISON', 60, 100, 89, 55, 69, 112, 0),
(183, 546, 'Cottonee', 'GRASSFAIRY', 40, 27, 60, 37, 50, 66, 0),
(184, 547, 'Whimsicott', 'GRASSFAIRY', 60, 67, 85, 77, 75, 116, 0),
(185, 548, 'Petilil', 'GRASS', 45, 35, 50, 70, 50, 30, 0),
(186, 549, 'Lilligant', 'GRASS', 70, 60, 75, 110, 75, 90, 0),
(187, 550, 'Basculin', 'WATER', 70, 92, 65, 80, 55, 98, 0),
(188, 550, '\"Basculin \r\n (Blue-Striped Form)\"', 'WATER', 70, 92, 65, 80, 55, 98, 0),
(189, 551, 'Sandile', 'GROUNDDARK', 50, 72, 35, 35, 35, 65, 0),
(190, 552, 'Krokorok', 'GROUNDDARK', 60, 82, 45, 45, 45, 74, 0),
(191, 553, 'Krookodile', 'GROUNDDARK', 95, 117, 80, 65, 70, 92, 0),
(192, 554, 'Darumaka', 'FIRE', 70, 90, 45, 15, 45, 50, 0),
(193, 555, 'Darmanitan', 'FIRE', 105, 140, 55, 30, 55, 95, 0),
(194, 555, '\"Darmanitan \r\n (Zen Mode)\"', 'FIREPSYCHC', 105, 30, 105, 140, 105, 55, 0),
(195, 556, 'Maractus', 'GRASS', 75, 86, 67, 106, 67, 60, 0),
(196, 557, 'Dwebble', 'BUGROCK', 50, 65, 85, 35, 35, 55, 0),
(197, 558, 'Crustle', 'BUGROCK', 70, 95, 125, 65, 75, 45, 0),
(198, 559, 'Scraggy', 'DARKFIGHT', 50, 75, 70, 35, 70, 48, 0),
(199, 560, 'Scrafty', 'DARKFIGHT', 65, 90, 115, 45, 115, 58, 0),
(200, 561, 'Sigilyph', 'PSYCHCFLYING', 72, 58, 80, 103, 80, 97, 0),
(201, 562, 'Yamask', 'GHOST', 38, 30, 85, 55, 65, 30, 0),
(202, 563, 'Cofagrigus', 'GHOST', 58, 50, 145, 95, 105, 30, 0),
(203, 564, 'Tirtouga', 'WATERROCK', 54, 78, 103, 53, 45, 22, 0),
(204, 565, 'Carracosta', 'WATERROCK', 74, 108, 133, 83, 65, 32, 0),
(205, 566, 'Archen', 'ROCKFLYING', 55, 112, 45, 74, 45, 70, 0),
(206, 567, 'Archeops', 'ROCKFLYING', 75, 140, 65, 112, 65, 110, 0),
(207, 568, 'Trubbish', 'POISON', 50, 50, 62, 40, 62, 65, 0),
(208, 569, 'Garbodor', 'POISON', 80, 95, 82, 60, 82, 75, 0),
(209, 570, 'Zorua', 'DARK', 40, 65, 40, 80, 40, 65, 0),
(210, 571, 'Zoroark', 'DARK', 60, 105, 60, 120, 60, 105, 0),
(211, 572, 'Minccino', 'NORMAL', 55, 50, 40, 40, 40, 75, 0),
(212, 573, 'Cinccino', 'NORMAL', 75, 95, 60, 65, 60, 115, 0),
(213, 574, 'Gothita', 'PSYCHC', 45, 30, 50, 55, 65, 45, 0),
(214, 575, 'Gothorita', 'PSYCHC', 60, 45, 70, 75, 85, 55, 0),
(215, 576, 'Gothitelle', 'PSYCHC', 70, 55, 95, 95, 110, 65, 0),
(216, 577, 'Solosis', 'PSYCHC', 45, 30, 40, 105, 50, 20, 0),
(217, 578, 'Duosion', 'PSYCHC', 65, 40, 50, 125, 60, 30, 0),
(218, 579, 'Reuniclus', 'PSYCHC', 110, 65, 75, 125, 85, 30, 0),
(219, 580, 'Ducklett', 'WATERFLYING', 62, 44, 50, 44, 50, 55, 0),
(220, 581, 'Swanna', 'WATERFLYING', 75, 87, 63, 87, 63, 98, 0),
(221, 582, 'Vanillite', 'ICE', 36, 50, 50, 65, 60, 44, 0),
(222, 583, 'Vanillish', 'ICE', 51, 65, 65, 80, 75, 59, 0),
(223, 584, 'Vanilluxe', 'ICE', 71, 95, 85, 110, 95, 79, 0),
(224, 585, 'Deerling', 'NORMALGRASS', 60, 60, 50, 40, 50, 75, 0),
(225, 586, 'Sawsbuck', 'NORMALGRASS', 80, 100, 70, 60, 70, 95, 0),
(226, 587, 'Emolga', 'ELECTRFLYING', 55, 75, 60, 75, 60, 103, 0),
(227, 588, 'Karrablast', 'BUG', 50, 75, 45, 40, 45, 60, 0),
(228, 589, 'Escavalier', 'BUGSTEEL', 70, 135, 105, 60, 105, 20, 0),
(229, 590, 'Foongus', 'GRASSPOISON', 69, 55, 45, 55, 55, 15, 0),
(230, 591, 'Amoonguss', 'GRASSPOISON', 114, 85, 70, 85, 80, 30, 0),
(231, 592, 'Frillish', 'WATERGHOST', 55, 40, 50, 65, 85, 40, 0),
(232, 593, 'Jellicent', 'WATERGHOST', 100, 60, 70, 85, 105, 60, 0),
(233, 594, 'Alomomola', 'WATER', 165, 75, 80, 40, 45, 65, 0),
(234, 595, 'Joltik', 'BUGELECTR', 50, 47, 50, 57, 50, 65, 0),
(235, 596, 'Galvantula', 'BUGELECTR', 70, 77, 60, 97, 60, 108, 0),
(236, 597, 'Ferroseed', 'GRASSSTEEL', 44, 50, 91, 24, 86, 10, 0),
(237, 598, 'Ferrothorn', 'GRASSSTEEL', 74, 94, 131, 54, 116, 20, 0),
(238, 599, 'Klink', 'STEEL', 40, 55, 70, 45, 60, 30, 0),
(239, 600, 'Klang', 'STEEL', 60, 80, 95, 70, 85, 50, 0),
(240, 601, 'Klinklang', 'STEEL', 60, 100, 115, 70, 85, 90, 0),
(241, 602, 'Tynamo', 'ELECTR', 35, 55, 40, 45, 40, 60, 0),
(242, 603, 'Eelektrik', 'ELECTR', 65, 85, 70, 75, 70, 40, 0),
(243, 604, 'Eelektross', 'ELECTR', 85, 115, 80, 105, 80, 50, 0),
(244, 605, 'Elgyem', 'PSYCHC', 55, 55, 55, 85, 55, 30, 0),
(245, 606, 'Beheeyem', 'PSYCHC', 75, 75, 75, 125, 95, 40, 0),
(246, 607, 'Litwick', 'GHOSTFIRE', 50, 30, 55, 65, 55, 20, 0),
(247, 608, 'Lampent', 'GHOSTFIRE', 60, 40, 60, 95, 60, 55, 0),
(248, 609, 'Chandelure', 'GHOSTFIRE', 60, 55, 90, 145, 90, 80, 0),
(249, 610, 'Axew', 'DRAGON', 46, 87, 60, 30, 40, 57, 0),
(250, 611, 'Fraxure', 'DRAGON', 66, 117, 70, 40, 50, 67, 0),
(251, 612, 'Haxorus', 'DRAGON', 76, 147, 90, 60, 70, 97, 0),
(252, 613, 'Cubchoo', 'ICE', 55, 70, 40, 60, 40, 40, 0),
(253, 614, 'Beartic', 'ICE', 95, 110, 80, 70, 80, 50, 0),
(254, 615, 'Cryogonal', 'ICE', 70, 50, 30, 95, 135, 105, 0),
(255, 616, 'Shelmet', 'BUG', 50, 40, 85, 40, 65, 25, 0),
(256, 617, 'Accelgor', 'BUG', 80, 70, 40, 100, 60, 145, 0),
(257, 618, 'Stunfisk', 'GROUNDELECTR', 109, 66, 84, 81, 99, 32, 0),
(258, 619, 'Mienfoo', 'FIGHT', 45, 85, 50, 55, 50, 65, 0),
(259, 620, 'Mienshao', 'FIGHT', 65, 125, 60, 95, 60, 105, 0),
(260, 621, 'Druddigon', 'DRAGON', 77, 120, 90, 60, 90, 48, 0),
(261, 622, 'Golett', 'GROUNDGHOST', 59, 74, 50, 35, 50, 35, 0),
(262, 623, 'Golurk', 'GROUNDGHOST', 89, 124, 80, 55, 80, 55, 0),
(263, 624, 'Pawniard', 'DARKSTEEL', 45, 85, 70, 40, 40, 60, 0),
(264, 625, 'Bisharp', 'DARKSTEEL', 65, 125, 100, 60, 70, 70, 0),
(265, 626, 'Bouffalant', 'NORMAL', 95, 110, 95, 40, 95, 55, 0),
(266, 627, 'Rufflet', 'NORMALFLYING', 70, 83, 50, 37, 50, 60, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_types_lnk`
--

CREATE TABLE `pokemon_types_lnk` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `pokemon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pokemon_types_lnk`
--

INSERT INTO `pokemon_types_lnk` (`id`, `type_id`, `pokemon_id`) VALUES
(790, 39, 17),
(791, 39, 18),
(792, 39, 19),
(793, 39, 20),
(794, 39, 21),
(795, 39, 48),
(796, 39, 51),
(797, 39, 52),
(798, 39, 53),
(799, 39, 56),
(800, 39, 57),
(801, 39, 65),
(802, 39, 66),
(803, 39, 72),
(804, 39, 91),
(805, 39, 102),
(806, 39, 120),
(807, 39, 129),
(808, 39, 140),
(809, 39, 141),
(810, 39, 142),
(811, 39, 143),
(812, 39, 144),
(813, 39, 155),
(814, 39, 156),
(815, 39, 157),
(816, 39, 167),
(817, 39, 168),
(818, 39, 211),
(819, 39, 212),
(820, 39, 224),
(821, 39, 225),
(822, 39, 265),
(823, 39, 266),
(853, 40, 12),
(854, 40, 13),
(855, 40, 53),
(856, 40, 73),
(857, 40, 74),
(858, 40, 75),
(859, 40, 80),
(860, 40, 81),
(861, 40, 103),
(862, 40, 104),
(863, 40, 135),
(864, 40, 136),
(865, 40, 169),
(866, 40, 170),
(867, 40, 171),
(868, 40, 175),
(869, 40, 176),
(870, 40, 198),
(871, 40, 199),
(872, 40, 258),
(873, 40, 259),
(884, 41, 17),
(885, 41, 18),
(886, 41, 19),
(887, 41, 37),
(888, 41, 38),
(889, 41, 39),
(890, 41, 49),
(891, 41, 50),
(892, 41, 55),
(893, 41, 66),
(894, 41, 85),
(895, 41, 96),
(896, 41, 97),
(897, 41, 100),
(898, 41, 112),
(899, 41, 128),
(900, 41, 155),
(901, 41, 156),
(902, 41, 157),
(903, 41, 163),
(904, 41, 164),
(905, 41, 200),
(906, 41, 205),
(907, 41, 206),
(908, 41, 219),
(909, 41, 220),
(910, 41, 226),
(911, 41, 266),
(915, 42, 27),
(916, 42, 28),
(917, 42, 59),
(918, 42, 60),
(919, 42, 78),
(920, 42, 79),
(921, 42, 80),
(922, 42, 81),
(923, 42, 180),
(924, 42, 181),
(925, 42, 182),
(926, 42, 207),
(927, 42, 208),
(928, 42, 229),
(929, 42, 230),
(931, 43, 35),
(932, 43, 47),
(933, 43, 68),
(934, 43, 69),
(935, 43, 70),
(936, 43, 71),
(937, 43, 76),
(938, 43, 77),
(939, 43, 92),
(940, 43, 100),
(941, 43, 101),
(942, 43, 165),
(943, 43, 166),
(944, 43, 173),
(945, 43, 174),
(946, 43, 189),
(947, 43, 190),
(948, 43, 191),
(949, 43, 257),
(950, 43, 261),
(951, 43, 262),
(961, 44, 29),
(962, 44, 30),
(963, 44, 31),
(964, 44, 32),
(965, 44, 63),
(966, 44, 92),
(967, 44, 105),
(968, 44, 160),
(969, 44, 161),
(970, 44, 162),
(971, 44, 196),
(972, 44, 197),
(973, 44, 203),
(974, 44, 204),
(975, 44, 205),
(976, 44, 206),
(992, 45, 22),
(993, 45, 23),
(994, 45, 33),
(995, 45, 34),
(996, 45, 35),
(997, 45, 36),
(998, 45, 37),
(999, 45, 38),
(1000, 45, 39),
(1001, 45, 78),
(1002, 45, 97),
(1003, 45, 177),
(1004, 45, 178),
(1005, 45, 179),
(1006, 45, 180),
(1007, 45, 181),
(1008, 45, 182),
(1009, 45, 196),
(1010, 45, 197),
(1011, 45, 227),
(1012, 45, 228),
(1013, 45, 234),
(1014, 45, 235),
(1015, 45, 255),
(1016, 45, 256),
(1023, 46, 49),
(1024, 46, 50),
(1025, 46, 54),
(1026, 46, 67),
(1027, 46, 106),
(1028, 46, 107),
(1029, 46, 108),
(1030, 46, 121),
(1031, 46, 122),
(1032, 46, 201),
(1033, 46, 202),
(1034, 46, 231),
(1035, 46, 232),
(1036, 46, 246),
(1037, 46, 247),
(1038, 46, 248),
(1039, 46, 261),
(1040, 46, 262),
(1054, 39, 17),
(1055, 39, 18),
(1056, 39, 19),
(1057, 39, 20),
(1058, 39, 21),
(1059, 39, 48),
(1060, 39, 51),
(1061, 39, 52),
(1062, 39, 53),
(1063, 39, 56),
(1064, 39, 57),
(1065, 39, 65),
(1066, 39, 66),
(1067, 39, 72),
(1068, 39, 91),
(1069, 39, 102),
(1070, 39, 120),
(1071, 39, 129),
(1072, 39, 140),
(1073, 39, 141),
(1074, 39, 142),
(1075, 39, 143),
(1076, 39, 144),
(1077, 39, 155),
(1078, 39, 156),
(1079, 39, 157),
(1080, 39, 167),
(1081, 39, 168),
(1082, 39, 211),
(1083, 39, 212),
(1084, 39, 224),
(1085, 39, 225),
(1086, 39, 265),
(1087, 39, 266),
(1117, 40, 12),
(1118, 40, 13),
(1119, 40, 53),
(1120, 40, 73),
(1121, 40, 74),
(1122, 40, 75),
(1123, 40, 80),
(1124, 40, 81),
(1125, 40, 103),
(1126, 40, 104),
(1127, 40, 135),
(1128, 40, 136),
(1129, 40, 169),
(1130, 40, 170),
(1131, 40, 171),
(1132, 40, 175),
(1133, 40, 176),
(1134, 40, 198),
(1135, 40, 199),
(1136, 40, 258),
(1137, 40, 259),
(1148, 41, 17),
(1149, 41, 18),
(1150, 41, 19),
(1151, 41, 37),
(1152, 41, 38),
(1153, 41, 39),
(1154, 41, 49),
(1155, 41, 50),
(1156, 41, 55),
(1157, 41, 66),
(1158, 41, 85),
(1159, 41, 96),
(1160, 41, 97),
(1161, 41, 100),
(1162, 41, 112),
(1163, 41, 128),
(1164, 41, 155),
(1165, 41, 156),
(1166, 41, 157),
(1167, 41, 163),
(1168, 41, 164),
(1169, 41, 200),
(1170, 41, 205),
(1171, 41, 206),
(1172, 41, 219),
(1173, 41, 220),
(1174, 41, 226),
(1175, 41, 266),
(1179, 42, 27),
(1180, 42, 28),
(1181, 42, 59),
(1182, 42, 60),
(1183, 42, 78),
(1184, 42, 79),
(1185, 42, 80),
(1186, 42, 81),
(1187, 42, 180),
(1188, 42, 181),
(1189, 42, 182),
(1190, 42, 207),
(1191, 42, 208),
(1192, 42, 229),
(1193, 42, 230),
(1194, 43, 10),
(1195, 43, 35),
(1196, 43, 47),
(1197, 43, 68),
(1198, 43, 69),
(1199, 43, 70),
(1200, 43, 71),
(1201, 43, 76),
(1202, 43, 77),
(1203, 43, 92),
(1204, 43, 100),
(1205, 43, 101),
(1206, 43, 165),
(1207, 43, 166),
(1208, 43, 173),
(1209, 43, 174),
(1210, 43, 189),
(1211, 43, 190),
(1212, 43, 191),
(1213, 43, 257),
(1214, 43, 261),
(1215, 43, 262),
(1225, 44, 29),
(1226, 44, 30),
(1227, 44, 31),
(1228, 44, 32),
(1229, 44, 63),
(1230, 44, 92),
(1231, 44, 105),
(1232, 44, 160),
(1233, 44, 161),
(1234, 44, 162),
(1235, 44, 196),
(1236, 44, 197),
(1237, 44, 203),
(1238, 44, 204),
(1239, 44, 205),
(1240, 44, 206),
(1256, 45, 22),
(1257, 45, 23),
(1258, 45, 33),
(1259, 45, 34),
(1260, 45, 35),
(1261, 45, 36),
(1262, 45, 37),
(1263, 45, 38),
(1264, 45, 39),
(1265, 45, 78),
(1266, 45, 97),
(1267, 45, 177),
(1268, 45, 178),
(1269, 45, 179),
(1270, 45, 180),
(1271, 45, 181),
(1272, 45, 182),
(1273, 45, 196),
(1274, 45, 197),
(1275, 45, 227),
(1276, 45, 228),
(1277, 45, 234),
(1278, 45, 235),
(1279, 45, 255),
(1280, 45, 256),
(1287, 46, 49),
(1288, 46, 50),
(1289, 46, 54),
(1290, 46, 67),
(1291, 46, 106),
(1292, 46, 107),
(1293, 46, 108),
(1294, 46, 121),
(1295, 46, 122),
(1296, 46, 201),
(1297, 46, 202),
(1298, 46, 231),
(1299, 46, 232),
(1300, 46, 246),
(1301, 46, 247),
(1302, 46, 248),
(1303, 46, 261),
(1304, 46, 262),
(1318, 47, 16),
(1319, 47, 31),
(1320, 47, 32),
(1321, 47, 36),
(1322, 47, 61),
(1323, 47, 62),
(1324, 47, 74),
(1325, 47, 75),
(1326, 47, 90),
(1327, 47, 105),
(1328, 47, 117),
(1329, 47, 119),
(1330, 47, 166),
(1331, 47, 228),
(1332, 47, 236),
(1333, 47, 237),
(1334, 47, 238),
(1335, 47, 239),
(1336, 47, 240),
(1337, 47, 263),
(1338, 47, 264),
(1349, 48, 11),
(1350, 48, 12),
(1351, 48, 13),
(1352, 48, 95),
(1353, 48, 109),
(1354, 48, 119),
(1355, 48, 130),
(1356, 48, 134),
(1357, 48, 135),
(1358, 48, 136),
(1359, 48, 149),
(1360, 48, 150),
(1361, 48, 192),
(1362, 48, 193),
(1363, 48, 194),
(1364, 48, 246),
(1365, 48, 247),
(1366, 48, 248),
(1380, 49, 14),
(1381, 49, 15),
(1382, 49, 16),
(1383, 49, 21),
(1384, 49, 41),
(1385, 49, 42),
(1386, 49, 46),
(1387, 49, 47),
(1388, 49, 83),
(1389, 49, 84),
(1390, 49, 85),
(1391, 49, 110),
(1392, 49, 118),
(1393, 49, 124),
(1394, 49, 125),
(1395, 49, 137),
(1396, 49, 138),
(1397, 49, 139),
(1398, 49, 151),
(1399, 49, 152),
(1400, 49, 172),
(1401, 49, 173),
(1402, 49, 174),
(1403, 49, 187),
(1404, 49, 188),
(1405, 49, 203),
(1406, 49, 204),
(1407, 49, 219),
(1408, 49, 220),
(1409, 49, 231),
(1410, 49, 232),
(1411, 49, 233),
(1443, 50, 8),
(1444, 50, 9),
(1445, 50, 10),
(1446, 50, 27),
(1447, 50, 28),
(1448, 50, 34),
(1449, 50, 43),
(1450, 50, 44),
(1451, 50, 45),
(1452, 50, 82),
(1453, 50, 86),
(1454, 50, 87),
(1455, 50, 88),
(1456, 50, 93),
(1457, 50, 98),
(1458, 50, 113),
(1459, 50, 127),
(1460, 50, 128),
(1461, 50, 131),
(1462, 50, 132),
(1463, 50, 133),
(1464, 50, 147),
(1465, 50, 148),
(1466, 50, 177),
(1467, 50, 178),
(1468, 50, 179),
(1469, 50, 183),
(1470, 50, 184),
(1471, 50, 185),
(1472, 50, 186),
(1473, 50, 195),
(1474, 50, 224),
(1475, 50, 225),
(1476, 50, 229),
(1477, 50, 230),
(1478, 50, 236),
(1479, 50, 237),
(1506, 51, 24),
(1507, 51, 25),
(1508, 51, 26),
(1509, 51, 40),
(1510, 51, 90),
(1511, 51, 94),
(1512, 51, 108),
(1513, 51, 109),
(1514, 51, 110),
(1515, 51, 111),
(1516, 51, 112),
(1517, 51, 113),
(1518, 51, 158),
(1519, 51, 159),
(1520, 51, 226),
(1521, 51, 234),
(1522, 51, 235),
(1523, 51, 241),
(1524, 51, 242),
(1525, 51, 243),
(1526, 51, 257),
(1537, 52, 58),
(1538, 52, 61),
(1539, 52, 62),
(1540, 52, 64),
(1541, 52, 103),
(1542, 52, 104),
(1543, 52, 114),
(1544, 52, 115),
(1545, 52, 116),
(1546, 52, 123),
(1547, 52, 130),
(1548, 52, 153),
(1549, 52, 154),
(1550, 52, 163),
(1551, 52, 164),
(1552, 52, 194),
(1553, 52, 200),
(1554, 52, 213),
(1555, 52, 214),
(1556, 52, 215),
(1557, 52, 216),
(1558, 52, 217),
(1559, 52, 218),
(1560, 52, 244),
(1561, 52, 245),
(1568, 53, 86),
(1569, 53, 87),
(1570, 53, 88),
(1571, 53, 89),
(1572, 53, 99),
(1573, 53, 101),
(1574, 53, 107),
(1575, 53, 111),
(1576, 53, 221),
(1577, 53, 222),
(1578, 53, 223),
(1579, 53, 252),
(1580, 53, 253),
(1581, 53, 254),
(1583, 54, 68),
(1584, 54, 69),
(1585, 54, 70),
(1586, 54, 71),
(1587, 54, 117),
(1588, 54, 118),
(1589, 54, 121),
(1590, 54, 122),
(1591, 54, 249),
(1592, 54, 250),
(1593, 54, 251),
(1594, 54, 260),
(1598, 54, 64),
(1599, 54, 96),
(1600, 54, 168),
(1601, 54, 183),
(1602, 54, 184),
(1605, 54, 55),
(1606, 54, 59),
(1607, 54, 60),
(1608, 54, 67),
(1609, 54, 79),
(1610, 54, 89),
(1611, 54, 126),
(1612, 54, 145),
(1613, 54, 146),
(1614, 54, 189),
(1615, 54, 190),
(1616, 54, 191),
(1617, 54, 198),
(1618, 54, 199),
(1619, 54, 209),
(1620, 54, 210),
(1621, 54, 263),
(1622, 54, 264);

-- --------------------------------------------------------

--
-- Table structure for table `resistant_to_type_lnk`
--

CREATE TABLE `resistant_to_type_lnk` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `resistant_to_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resistant_to_type_lnk`
--

INSERT INTO `resistant_to_type_lnk` (`id`, `type_id`, `resistant_to_type_id`) VALUES
(384, 39, 46),
(385, 40, 44),
(386, 40, 45),
(387, 40, 56),
(388, 41, 40),
(389, 41, 43),
(390, 41, 45),
(391, 41, 50),
(395, 42, 40),
(396, 42, 42),
(397, 42, 50),
(398, 42, 55),
(402, 43, 42),
(403, 43, 44),
(404, 43, 51),
(405, 44, 39),
(406, 44, 41),
(407, 44, 42),
(408, 44, 48),
(412, 45, 40),
(413, 45, 43),
(414, 45, 50),
(415, 46, 39),
(416, 46, 40),
(417, 46, 42),
(418, 46, 45),
(422, 47, 39),
(423, 47, 41),
(424, 47, 42),
(425, 47, 44),
(426, 47, 45),
(427, 47, 47),
(428, 47, 50),
(429, 47, 52),
(430, 47, 53),
(431, 47, 54),
(432, 47, 55),
(437, 48, 45),
(438, 48, 47),
(439, 48, 48),
(440, 48, 50),
(441, 48, 53),
(444, 49, 47),
(445, 49, 48),
(446, 49, 49),
(447, 49, 53),
(451, 50, 43),
(452, 50, 49),
(453, 50, 50),
(454, 50, 51),
(458, 51, 41),
(459, 51, 47),
(460, 51, 51),
(461, 52, 40),
(462, 52, 52),
(464, 53, 53),
(465, 54, 48),
(466, 54, 49),
(467, 54, 50),
(468, 54, 51),
(472, 55, 40),
(473, 55, 45),
(474, 55, 54),
(475, 55, 56),
(479, 56, 46),
(480, 56, 52),
(481, 56, 56);

-- --------------------------------------------------------

--
-- Table structure for table `strong_against_type_lnk`
--

CREATE TABLE `strong_against_type_lnk` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `strong_against_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `strong_against_type_lnk`
--

INSERT INTO `strong_against_type_lnk` (`id`, `type_id`, `strong_against_type_id`) VALUES
(75, 40, 39),
(76, 40, 44),
(77, 40, 47),
(78, 40, 53),
(79, 40, 56),
(82, 41, 40),
(83, 41, 45),
(84, 41, 50),
(85, 42, 50),
(86, 42, 55),
(88, 43, 42),
(89, 43, 44),
(90, 43, 47),
(91, 43, 48),
(92, 43, 51),
(95, 44, 41),
(96, 44, 45),
(97, 44, 48),
(98, 44, 53),
(102, 45, 50),
(103, 45, 52),
(104, 45, 56),
(105, 46, 46),
(106, 46, 52),
(108, 47, 44),
(109, 47, 53),
(110, 47, 55),
(111, 48, 45),
(112, 48, 47),
(113, 48, 50),
(114, 48, 53),
(118, 49, 43),
(119, 49, 44),
(120, 49, 48),
(121, 50, 43),
(122, 50, 44),
(123, 50, 49),
(124, 51, 41),
(125, 51, 49),
(127, 52, 40),
(128, 52, 42),
(130, 53, 41),
(131, 53, 43),
(132, 53, 50),
(133, 53, 54),
(137, 54, 54),
(138, 55, 40),
(139, 55, 54),
(140, 55, 56),
(141, 56, 46),
(142, 56, 52);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type_name`) VALUES
(39, 'NORMAL'),
(40, 'FIGHTING'),
(41, 'FLYING'),
(42, 'POISON'),
(43, 'GROUND'),
(44, 'ROCK'),
(45, 'BUG'),
(46, 'GHOST'),
(47, 'STEEL'),
(48, 'FIRE'),
(49, 'WATER'),
(50, 'GRASS'),
(51, 'ELECTRIC'),
(52, 'PSYCHIC'),
(53, 'ICE'),
(54, 'DRAGON'),
(55, 'FAIRY'),
(56, 'DARK');

-- --------------------------------------------------------

--
-- Table structure for table `vulnerable_to_type_lnk`
--

CREATE TABLE `vulnerable_to_type_lnk` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `vulnerable_to_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vulnerable_to_type_lnk`
--

INSERT INTO `vulnerable_to_type_lnk` (`id`, `type_id`, `vulnerable_to_type_id`) VALUES
(1, 39, 40),
(2, 40, 41),
(3, 40, 52),
(4, 40, 55),
(5, 41, 44),
(6, 41, 51),
(7, 41, 53),
(8, 42, 43),
(9, 42, 52),
(11, 43, 49),
(12, 43, 50),
(13, 43, 53),
(14, 44, 40),
(15, 44, 43),
(16, 44, 47),
(17, 44, 49),
(18, 44, 50),
(21, 45, 41),
(22, 45, 44),
(23, 45, 48),
(24, 46, 46),
(25, 46, 56),
(27, 47, 40),
(28, 47, 43),
(29, 47, 48),
(30, 48, 43),
(31, 48, 44),
(32, 48, 49),
(33, 49, 50),
(34, 49, 51),
(36, 50, 41),
(37, 50, 42),
(38, 50, 45),
(39, 50, 48),
(40, 50, 53),
(43, 51, 43),
(44, 52, 45),
(45, 52, 46),
(46, 52, 56),
(47, 53, 40),
(48, 53, 44),
(49, 53, 47),
(50, 53, 48),
(54, 54, 53),
(55, 54, 54),
(56, 54, 55),
(57, 55, 42),
(58, 55, 47),
(60, 56, 40),
(61, 56, 45),
(62, 56, 55);

-- --------------------------------------------------------

--
-- Table structure for table `weak_against_type_lnk`
--

CREATE TABLE `weak_against_type_lnk` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `weak_against_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weak_against_type_lnk`
--

INSERT INTO `weak_against_type_lnk` (`id`, `type_id`, `weak_against_type_id`) VALUES
(47, 39, 44),
(48, 39, 46),
(49, 39, 47),
(50, 40, 41),
(51, 40, 42),
(52, 40, 45),
(53, 40, 46),
(54, 40, 52),
(55, 40, 55),
(57, 41, 44),
(58, 41, 47),
(59, 41, 51),
(60, 42, 42),
(61, 42, 43),
(62, 42, 44),
(63, 42, 46),
(64, 42, 47),
(67, 43, 41),
(68, 43, 45),
(69, 43, 50),
(70, 44, 40),
(71, 44, 43),
(72, 44, 47),
(73, 45, 40),
(74, 45, 41),
(75, 45, 42),
(76, 45, 46),
(77, 45, 47),
(78, 45, 48),
(79, 45, 55),
(80, 46, 39),
(81, 46, 56),
(83, 47, 47),
(84, 47, 48),
(85, 47, 49),
(86, 47, 51),
(90, 48, 44),
(91, 48, 48),
(92, 48, 49),
(93, 48, 54),
(97, 49, 49),
(98, 49, 50),
(99, 49, 54),
(100, 50, 41),
(101, 50, 42),
(102, 50, 45),
(103, 50, 47),
(104, 50, 48),
(105, 50, 50),
(106, 50, 54),
(107, 51, 43),
(108, 51, 50),
(109, 51, 51),
(110, 51, 54),
(114, 52, 47),
(115, 52, 52),
(116, 52, 56),
(117, 53, 47),
(118, 53, 48),
(119, 53, 49),
(120, 53, 53),
(124, 54, 47),
(125, 54, 55),
(127, 55, 42),
(128, 55, 47),
(129, 55, 48),
(130, 56, 40),
(131, 56, 55),
(132, 56, 56);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pokemon_types_lnk`
--
ALTER TABLE `pokemon_types_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id_index` (`type_id`),
  ADD KEY `pokemon_id_index` (`pokemon_id`);

--
-- Indexes for table `resistant_to_type_lnk`
--
ALTER TABLE `resistant_to_type_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `resistant_to_type_id` (`resistant_to_type_id`);

--
-- Indexes for table `strong_against_type_lnk`
--
ALTER TABLE `strong_against_type_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `strong_against_type_id` (`strong_against_type_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vulnerable_to_type_lnk`
--
ALTER TABLE `vulnerable_to_type_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `vulnerable_to_type_id` (`vulnerable_to_type_id`);

--
-- Indexes for table `weak_against_type_lnk`
--
ALTER TABLE `weak_against_type_lnk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `weak_against_type_id` (`weak_against_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `pokemon_types_lnk`
--
ALTER TABLE `pokemon_types_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1623;

--
-- AUTO_INCREMENT for table `resistant_to_type_lnk`
--
ALTER TABLE `resistant_to_type_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=482;

--
-- AUTO_INCREMENT for table `strong_against_type_lnk`
--
ALTER TABLE `strong_against_type_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `vulnerable_to_type_lnk`
--
ALTER TABLE `vulnerable_to_type_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `weak_against_type_lnk`
--
ALTER TABLE `weak_against_type_lnk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
