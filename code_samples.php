<script type="text/javascript">

//code samples


//calling ajax (mine)

function createMessageDivs(row){//make a div from the row passed in
    let msg = "";
    
    msg = "<div class='row'><b>TIME:</b>&nbsp;&nbsp;&nbsp;"+row.time + "&nbsp;&nbsp;&nbsp;<b>USER:</b>&nbsp;&nbsp;&nbsp;" +row.username+ "&nbsp;&nbsp;&nbsp;<b>MESSAGE:</b>&nbsp;&nbsp;&nbsp;" +row.message + "</div>";
    
    return msg;
}



function getLatestMessages(){//fetch 10 most recent message in the last hour
  //alert("getlatestmesages called");

  let user = $("#username").val();
  let last_message_time = $("#last_message_time").val();
  console.log(user);
  
    $.ajax({
        url: "chat.php",
        method: "POST",
        data: {
            get_latest_msgs : "true",
            user: user,
            last_message_time: last_message_time,
            
        },
        success: function(msg){
          console.log(msg);
          messageIndex = JSON.parse(msg);
          console.log(messageIndex.length);
          let output = "";  
          for(let i = 0; i < messageIndex.length; i++){
              console.log(messageIndex[i]);
              output += createMessageDivs(messageIndex[i]);
          }
  
          $("#last_message_time").val(messageIndex[0].time);
  
          //let existingMSGS = $(".message_list_last_hour").html();//get the existing messages
          let existingMSGS = $(".message_list").html();//get the existing messages
        
          $(".message_list").html(output+existingMSGS);//need to add the newest message div to the top of existing messages  
        }
    })

    
}

$(".send_message").click(function(event){//add click event code to the to send msg button

  //console.log("send msg button clicked");
  let user = $("#username").val();//username is a text field with id username
  let msg = $("#message").val();

  sendMsg(user,msg);

}); 

$(".menuCover").click(toggleAnimationClasses);//another way to assign a function to a button


let messageUpdater = setInterval(getLatestMessages, 10000);//set a js function to get called repeatedly on an interval

$( document ).ready(function() {//event called once the document has finished loading
    console.log("document ready");
    getUserMessages();
});


function toggleAnimationClasses(event) {   //toggling classes on an event to show hide or trigger ones associated with animation
    let menu = $(".menu");
    let menuCover = $(".menuCover");
    let bullet = $(".bullet");
    if (menu.hasClass("open")) {
        menu.removeClass("open");
        menu.addClass("close");
        menuCover.removeClass("rotateUP");
        menuCover.addClass("rotateDOWN");
        bullet.removeClass("setBullet");
    } else {
        menu.addClass("open");
        menu.removeClass("close");
        menuCover.addClass("rotateUP");
        menuCover.removeClass("rotateDOWN");
        bullet.addClass("setBullet");
    }
}


$( ".menu" ).children().click(function ( event ) {//assign a click event to all the children inside this div

    let myvar = 0;
    
    $('.bullet').css({ 
        position: "absolute",
        marginLeft: 0, marginTop: 0,
        top: 0, left: myvar
    }).appendTo('body');

    let target_id = $( event.target ).attr('id');

    $('.bullet').bind("animationend", (event)=>{ //binding this event to the class that has the animation //need to use the animationend event for opening the url because it happens too fast otherwise! 
            let urls = new Array("http://www.cnn.com", "http://www.langara.ca","www.theglobeandmail.com","www.nationalpost.com","www.tennis.com");
            $(location).attr('href',urls[target_id]);
    })
    console.log("bullet_animation"+target_id);
    $('.bullet').addClass("bullet_animation"+target_id);

});



</script>


<?php

if(array_key_exists("get_last_10_msgs", $_POST) && array_key_exists("user", $_POST) ){//php code fetch 10 most recent message in the last hour  and return as json

    $username = $conn->real_escape_string($_POST['user']);
    $sql = "CALL getLast10Messages('".$username."')";
                           
    $result = $conn->query($sql);
    clearConnection($conn);

    if($result){
        
        $messages = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($messages);
            
    }else{
        echo "no result";
    }

}



//some less code to have a class that is hidden at first and has an animation associated with it
/*
.special_goodbye{
    background-color:black;
    display:none;
    //background-image:url("images/goodbye_bike.jpeg");
    background-repeat: no-repeat;
    background-size: 300px 100px;
    position: absolute;
    animation-name: moveBike;
    animation-duration: 3s;
    animation-timing-function: linear;

}

@keyframes moveBike { 
    0%  {left:0px;
              
    }

    50%  {left:500px;
              
    }

    75%  {left:850px;
             //top:50px;    
    }

    100%  {left:2050px;
            
    }
}
*/



?>






