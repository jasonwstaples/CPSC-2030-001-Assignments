<?php
session_start();//necessary to use $_SESSION variable on the page


function get_types_array($conn,$types_sql){
    
    
    $types_result = $conn->query($types_sql);
    clearConnection($conn);

    $types_for_twig = array();
    if($types_result){
        $types = $types_result->fetch_all(MYSQLI_ASSOC);
    
        //print_r($types);
        foreach ($types as $type) {
           $types_for_twig[] = $type['type_name'];

        }
        return $types_for_twig;
    }       
}



?>


<html>
	<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CPSC 2030 - Assignment 6 - Jason Staples - Langara ID# 100269858</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet/less" type="text/css" media="screen" href="fave_pokemon.less" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    </head>

    <body>

    	<div class="container">
            <?php
            
           require_once 'sqlhelper.php';
           require_once './vendor/autoload.php';  //include the twig library.
           

            $twig = setupMyTwigEnvironment(); //twig setup code taken from Kim's course example
            $conn = connectToMyDatabase(); //database setup code taken from Kim's course example
           ?>

    		<div class="header">
                <?php  
                    $header = $twig->load('header.twig.html');
                    echo $header->render(array("title"=>"Favorite Pokemon App")); 
                ?>
            </div>

    		<div class="sidebar">
                <?php  
                    $sidebar = $twig->load('sidebar.twig.html');
                    //popular gen 4 pokemon taken from here -> http://www.dorkly.com/post/56572/toplist-results-the-15-greatest-generation-iv-pokemon/page:4
                    $popular_pokemon_names = array();
                    $popular_pokemon_names[] = 'Lucario';
                    $popular_pokemon_names[] = 'Garchomp';
                    $popular_pokemon_names[] = 'Darkrai';
                    $popular_pokemon_names[] = 'Arceus';

                    echo $sidebar->render(array("popular_pokemon"=>$popular_pokemon_names)); 
                ?>
            </div>

    		<div class="favorites">
                <?php 
                    $pokemon_table = $twig->load('favorites.twig.html');

                    if(!empty($_GET['add_favorite_id'])){
                        if(empty($_SESSION['favorite_pokemon_ids'])){
                            $_SESSION['favorite_pokemon_ids'] = array($_GET['add_favorite_id']);
                        }elseif(in_array($_GET['add_favorite_id'],$_SESSION['favorite_pokemon_ids'])){  
                            
                            $pos = array_search($_GET['add_favorite_id'],$_SESSION['favorite_pokemon_ids']);

                            unset($_SESSION['favorite_pokemon_ids'][$pos]);  

                        }elseif(count($_SESSION['favorite_pokemon_ids']) < 7){
                            $_SESSION['favorite_pokemon_ids'][] = $_GET['add_favorite_id'];
                            
                        }
                    }

                    //build the associative array data for all the favorite pokemon cards used by the twig
                    if(!empty($_SESSION['favorite_pokemon_ids'])){
                        $favorite_pokemon_data = array();
                        foreach ($_SESSION['favorite_pokemon_ids'] as $favorite_pokemon_id) {
                            $favorite_pokemon_id = $conn->real_escape_string($favorite_pokemon_id);
                            $sql = "CALL get_pokemon('','',".$favorite_pokemon_id.")";
                           
                            $pokemon_result = $conn->query($sql);
                            clearConnection($conn);

                            if($pokemon_result){
                                $myVar = array();

                                $myVar[] =  $pokemon_result->fetch_all(MYSQLI_ASSOC)[0];
                                
                                if($myVar[0]["mega_evolution_id"] > 0){
                                    $sql = "CALL get_pokemon('','',".$myVar[0]["mega_evolution_id"].")";
                                    $mega_evolution_result = $conn->query($sql);
                                    clearConnection($conn);
                                    $mega_form_name = '';
                                    if ($mega_evolution_result->num_rows > 0) {
                                        $mega_form_row =  $mega_evolution_result->fetch_assoc();
                                        $mega_form_name = $mega_form_row['name'];
                                        $myVar[0]['mega_form_name'] = $mega_form_row['name'];

                                    }   
                                }


                                //Output Strong against, Weak against, Resistant To, and Vulnerable To.
                                $types_sql = 'CALL get_pokemon_types('.$myVar[0]["id"].')';
                                //echo "<BR>".$types_sql;
                                $types_result = $conn->query($types_sql);
                                clearConnection($conn);

                                $types_for_twig = array();
                                if($types_result){
                                    $types = $types_result->fetch_all(MYSQLI_ASSOC);
                                    

                                    //print_r($types);
                                    $strong_against = array();
                                    $weak_against = array();
                                    $resistant_to = array();
                                    $vulnerable_to = array();
                                    
                                    foreach ($types as $type) {
                                       $types_for_twig[] = $type['type_name'];
                                       //get Strong against, Weak against, Resistant To, and Vulnerable To for this type
                                        $strong_against = array_merge($strong_against,get_types_array($conn,'CALL get_strong_against_types('.$type['type_id'].')'));
                                        $weak_against = array_merge($weak_against,get_types_array($conn,'CALL get_weak_against_types('.$type['type_id'].')'));
                                        $resistant_to = array_merge($resistant_to,get_types_array($conn,'CALL get_resistant_to_types('.$type['type_id'].')'));
                                        $vulnerable_to = array_merge($vulnerable_to,get_types_array($conn,'CALL get_vulnerable_to_types('.$type['type_id'].')')); 

                                    }


                                }    
                                

                                $myVar[0]['types']  = $types_for_twig;  
                                $myVar[0]['strong_against'] = $strong_against;
                                $myVar[0]['weak_against'] = $weak_against;
                                $myVar[0]['resistant_to'] = $resistant_to;
                                $myVar[0]['vulnerable_to'] = $vulnerable_to;   
                                
                                

                                

                                //print_r($myVar);   


                                $favorite_pokemon_data[] = $myVar[0];    

                               
                            }    
                        }  

                        echo $pokemon_table->render(array("favorite_pokemon"=>$favorite_pokemon_data)); 
                    }else{
                         echo $pokemon_table->render(array("favorite_pokemon"=>'')); 
                    }    


                ?>
            </div>

    		<div class="pokemon_table">

                <?php  
                    $pokemon_table = $twig->load('pokemon_table.twig.html');

                    $sql = "CALL get_pokemon_list(0)";
                    $pokemon_result = $conn->query($sql);
                    clearConnection($conn);

                    if($pokemon_result){
                        $table = $pokemon_result->fetch_all(MYSQLI_ASSOC);
                        //print_r($table);
                        if(!empty($_SESSION['favorite_pokemon_ids'])){
                            echo $pokemon_table->render(array("pokemon_table"=>$table,"favorite_pokemon_ids"=>$_SESSION['favorite_pokemon_ids'],"popular_pokemon_names"=>$popular_pokemon_names));
                        }else{
                            echo $pokemon_table->render(array("pokemon_table"=>$table,"favorite_pokemon_ids"=>'',"popular_pokemon_names"=>$popular_pokemon_names));
                        }    
                    }else{

                    }
                   
                ?>


            </div>
    		

    	</div>



    </body>
    


<html>    	