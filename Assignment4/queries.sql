USE pokedex;

#Bottom 10 pokemon ranked by speed
SELECT name FROM pokemon order by speed asc limit 10;

#All Pokémon who is vulnerable to Ground and Resistant to Steel
SELECT DISTINCT name FROM (

	SELECT  pokemon.name FROM pokemon 
	INNER JOIN pokemon_types_lnk ON pokemon.id = pokemon_types_lnk.pokemon_id
	INNER JOIN type ON pokemon_types_lnk.type_id = type.id
	WHERE type.id IN (
    
    	SELECT vulnerable_to_type_lnk.type_id from vulnerable_to_type_lnk INNER JOIN type ON vulnerable_to_type_lnk.vulnerable_to_type_id = type.id and type.type_name = 'GROUND'
    
	) 
    
)    as table1
WHERE name IN (

	SELECT pokemon.name FROM pokemon 
	INNER JOIN pokemon_types_lnk ON pokemon.id = pokemon_types_lnk.pokemon_id
	INNER JOIN type ON pokemon_types_lnk.type_id = type.id
	WHERE type.id IN (
    
    	SELECT resistant_to_type_lnk.type_id from resistant_to_type_lnk INNER JOIN type ON resistant_to_type_lnk.resistant_to_type_id = type.id and type.type_name = 'STEEL'
    
	) 
    
);



#Find All Pokémon who has a BST between 200 to 500 who is weak against water  
SELECT DISTINCT name from (
	SELECT * FROM (
    		SELECT (attack + defense+hp+special_attack+special_defense+speed) as BST, name from pokemon 
		) as innertable WHERE BST > 200 AND BST < 500
	) as outertable
WHERE name IN (
   	SELECT pokemon.name FROM pokemon 
	INNER JOIN pokemon_types_lnk ON pokemon.id = pokemon_types_lnk.pokemon_id
	INNER JOIN type ON pokemon_types_lnk.type_id = type.id
	WHERE type.id IN (

	    SELECT weak_against_type_lnk.type_id from weak_against_type_lnk INNER JOIN type ON weak_against_type_lnk.weak_against_type_id = type.id and type.type_name = 'WATER' 
	) 
);

#Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire
SELECT name  from pokemon
WHERE name IN (
 
	SELECT  pokemon.name FROM pokemon 
	INNER JOIN pokemon_types_lnk ON pokemon.id = pokemon_types_lnk.pokemon_id
	INNER JOIN type ON pokemon_types_lnk.type_id = type.id
	WHERE type.id IN (
    
    	SELECT vulnerable_to_type_lnk.type_id from vulnerable_to_type_lnk INNER JOIN type ON vulnerable_to_type_lnk.vulnerable_to_type_id = type.id and type.type_name = 'FIRE'
    
	) 
) AND mega_evolution_id > 0
ORDER BY attack DESC
limit 1;
