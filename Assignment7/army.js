


class Soldier{


    constructor(Name,Side,Unit,Rank,Role,Description,ImageURL){
        this.Name = Name;
        this.Side = Side;
        this.Unit = Unit;
        this.Rank = Rank;
        this.Description = Description;
        this.ImageURL = ImageURL;

    }

}



function addToSquadWithObject(aSoldierObject){//change this method to pass all the information required i.e. the soldier object
   
    let squadContainerDiv = document.querySelector("#squad_id");//get the squad container div
    //check to see if less than 5 soldiers already in the squad container div
    let soldiers = squadContainerDiv.querySelectorAll(".squad_link");

    
    if(soldiers.length < 5){//less than 5 soldiers in squad so possibly add to squad

        //only add soldier if not already in the squad
       soldierExistsAlready = false; 
       soldiers.forEach(function(soldier) {
            
            if(soldier.textContent == aSoldierObject.Name){
                console.log('exists already ' + aSoldierObject.Name);
                soldierExistsAlready = true;
            }
        });
     
        if(!soldierExistsAlready){

             //highlight the clicked soldier name
            let soldierDivID = aSoldierObject.Name.split(' ').join('_'); 
            let soldierLinkElement = document.querySelector("#"+soldierDivID);
            soldierLinkElement.setAttribute("class", "soldier_link_highlight");


            let newDiv = document.createElement("div"); 
            newDiv.setAttribute("class", "squad_link");
           
            let soldierName = document.createTextNode(aSoldierObject.Name); 
            
            
            newDiv.appendChild(soldierName);     
            newDiv.addEventListener('mouseover', function () {
                
                showProfile(aSoldierObject);    
            });  

            newDiv.addEventListener('click', function () {
                removeFromSquad(aSoldierObject.Name); 
            });
            squadContainerDiv.appendChild(newDiv);  
        }    

    }
}


function removeFromSquad(aSoldierName){
    
    //remove highlight in the sidebar based on the clicked soldier name
    let soldierDivID = aSoldierName.split(' ').join('_');
    let soldierLinkElement = document.querySelector("#"+soldierDivID);
    soldierLinkElement.setAttribute("class", "soldier_link");

    let squadContainerDiv = document.querySelector("#squad_id");//get the squad container div

    let soldiers = squadContainerDiv.querySelectorAll(".squad_link");

    soldiers.forEach(function(soldier) {
           
        if(soldier.textContent == aSoldierName){
            
            squadContainerDiv.removeChild(soldier);
                
        }
    });

}  

function showProfile(aSoldier){
   

    document.querySelector("#profile_name").innerHTML = aSoldier.Name;
    document.querySelector("#profile_side").innerHTML = aSoldier.Side;
    document.querySelector("#profile_unit").innerHTML = aSoldier.Unit;
    document.querySelector("#profile_rank").innerHTML = aSoldier.Rank;
    document.querySelector("#profile_description").innerHTML = aSoldier.Description;

    let the_image = document.querySelector("#profile_picture");
    
    the_image.src = "images/"+aSoldier.ImageURL;

   
} 

function getSoldierObject(aSoldierName){
    
    let soldierArray = buildSoldierArray();

    for(let soldier of soldierArray){

        if(soldier.Name == aSoldierName){
            
            return soldier;

        }

    }    


}


function buildSoldierArray(){

    let soldierArray = [];
    const soldier_1 = new Soldier('Claude Wallace','Edinburgh Army','Ranger Corps, Squad E','First Lieutenant','Tank Commander','Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.','claude_wallace.png');
    soldierArray.push(soldier_1);
    const soldier_2 = new Soldier('Riley Miller','Edinburgh Army','Federate Joint Ops','Second Lieutenant','Artillery Advisor','Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.','riley_miller.png');
    soldierArray.push(soldier_2);
    const soldier_3 = new Soldier('Raz','Edinburgh Army','Ranger Corps, Squad E','Sergeant','Fireteam Leader',"Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible",'raz.png');   
    soldierArray.push(soldier_3);
    const soldier_4 = new Soldier('Kai Shulen','Edinburgh Army','Ranger Corps, Squad E','Sergeant Major','Fireteam Leader','Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename "Deadeye Kai." Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.','kai_shulen.png');
    soldierArray.push(soldier_4);
    const soldier_5 = new Soldier('Angelica Farnaby','','','','','A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed "Angie," she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name.','angelica_farnaby.png');
    soldierArray.push(soldier_5);
    const soldier_6 = new Soldier('Minerva Victor','Edinburgh Army','Ranger Corps, Squad F','First Lieutenant','Senior Commander',"Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",'minerva_victor.png');
    soldierArray.push(soldier_6);
    const soldier_7 = new Soldier('Karen Stuart','Edinburgh Army','Squad E','Corporal','Combat EMT',"Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",'karen_stuart.png');
    soldierArray.push(soldier_7);
    const soldier_8 = new Soldier('Ragnarok','Edinburgh Army','Squad E','K-9 Unit','Mascot',"Once a stray, this good good boy is lovingly referred to as 'Rags'. As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",'ragnarok.png');
    soldierArray.push(soldier_8);
    const soldier_9 = new Soldier('Miles Arbeck','Edinburgh Army','Ranger Corps, Squad E','Sergeant','Tank Operator',"Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",'miles_arbeck.png');
    soldierArray.push(soldier_9);
    const soldier_10 = new Soldier('Dan Bentley','Edinburgh Army','Ranger Corps, Squad E','Private First Class','APC Operator',"Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.",'dan_bentley.png');
    soldierArray.push(soldier_10);
    
    return soldierArray;

}


function addSoldierLinks(){
   
    

    let soldierArray = buildSoldierArray();


    for(let soldier of soldierArray){
        
        let baseElement = document.querySelector("#army_links");
        let newDiv = document.createElement("div"); 
        newDiv.setAttribute("class", "soldier_link");
        
        let soldierDivID = soldier.Name.split(' ').join('_');
        newDiv.setAttribute("id", soldierDivID);

        let soldierName = document.createTextNode(soldier.Name); 
        
        newDiv.addEventListener('click', function () {
            addToSquadWithObject(soldier);    
        });

        newDiv.addEventListener('mouseover', function () {
            showProfile(soldier);    
        });

        newDiv.appendChild(soldierName);       
        baseElement.appendChild(newDiv);  
    }    
   

}