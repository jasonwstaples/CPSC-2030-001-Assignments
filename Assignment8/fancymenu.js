function toggleAnimationClasses(event) {   
    let menu = $(".menu");
    let menuCover = $(".menuCover");
    let bullet = $(".bullet");
    if (menu.hasClass("open")) {
        menu.removeClass("open");
        menu.addClass("close");
        menuCover.removeClass("rotateUP");
        menuCover.addClass("rotateDOWN");
        bullet.removeClass("setBullet");
    } else {
        menu.addClass("open");
        menu.removeClass("close");
        menuCover.addClass("rotateUP");
        menuCover.removeClass("rotateDOWN");
        bullet.addClass("setBullet");
    }
}



$(".menuCover").click(toggleAnimationClasses);

$( ".menu" ).children().click(function ( event ) {

    let myvar = 0;
    
    $('.bullet').css({ 
        position: "absolute",
        marginLeft: 0, marginTop: 0,
        top: 0, left: myvar
    }).appendTo('body');

    let target_id = $( event.target ).attr('id');

    $('.bullet').bind("animationend", (event)=>{ //need to use the animationend event for opening the url because it happens too fast otherwise!
            let urls = new Array("http://www.cnn.com", "http://www.langara.ca","www.theglobeandmail.com","www.nationalpost.com","www.tennis.com");
            $(location).attr('href',urls[target_id]);
    })
    console.log("bullet_animation"+target_id);
    $('.bullet').addClass("bullet_animation"+target_id);

});




